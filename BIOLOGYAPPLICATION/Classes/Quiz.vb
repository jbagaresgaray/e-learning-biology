
'##################################################################
'###  This module contains the following classes:               ###
'###                                                            ###
'###  Quiz: represents a quiz session/also  can be used by      ###
'###  programmers to add,delete questions to the quiz database. ###
'###                                                            ###
'###  QuizQuestion: represents a single question                ###
'###                                                            ###
'###  QuizQuestion.Options: Nested class, which holds a set of  ###
'###  4 options for each QuizQuestion object.                   ###
'###                                                            ###
'###  For a demonstration on how to use these classes & their   ###
'###  properties, methods, and events refer to the forms asso-  ###
'###  ciated with this project.                                 ###
'##################################################################

'we use binary serialization to persist questions to a disk file
Imports System.Runtime.Serialization.Formatters.Binary
'system.io is used to access the file system
Imports System.IO

'QuizQuestion Class
'represents a single question during a quiz session
'note that its serializable, meaning it can store or retrieve its
'objects(ie questions) from/to a disk file (ie quiz.bin)
<Serializable()> Public Class QuizQuestion

    'Options Nested Class
    'represents a set of four options in single question object
    <Serializable()> Public Class Options
        Private Options(4) As String

        'constructor
        Sub New()
            MyBase.New()
        End Sub

        'overloaded constructor which lets the programmer specify all
        'the four options during declaration of the object
        Sub New(ByVal Option1 As String, ByVal Option2 As String, ByVal Option3 As String, ByVal option4 As String)
            MyBase.New()
            Options(0) = Option1
            Options(1) = Option2
            Options(2) = Option3
            Options(3) = option4
        End Sub

        Property Option1() As String
            Get
                Option1 = Options(0)
            End Get
            Set(ByVal value As String)
                Options(0) = value
            End Set
        End Property

        Property Option2() As String
            Get
                Option2 = Options(1)
            End Get
            Set(ByVal value As String)
                Options(1) = value
            End Set
        End Property
        Property Option3() As String
            Get
                Option3 = Options(2)
            End Get
            Set(ByVal value As String)
                Options(2) = value
            End Set
        End Property
        Property Option4() As String
            Get
                Option4 = Options(3)
            End Get
            Set(ByVal value As String)
                Options(3) = value
            End Set
        End Property

    End Class

    Private _Question As String         'stores a question
    Private _CorrectAnswer As String    'stores the correct answer
    Private _QuestionOptions As Options 'stores an options object
    '                                    holding the question's 4 options

    Sub New()
        MyBase.New()
    End Sub

    Sub New(ByVal strQuestion As String)
        MyBase.New()
        _Question = strQuestion
    End Sub

    Property Question() As String
        Get
            Question = _Question
        End Get
        Set(ByVal value As String)
            _Question = value
        End Set
    End Property

    Property CorrectAnswer() As String
        Get
            CorrectAnswer = _CorrectAnswer
        End Get
        Set(ByVal value As String)
            _CorrectAnswer = value
        End Set
    End Property

    Property QuestionOptions() As Options
        Get
            QuestionOptions = _QuestionOptions
        End Get
        Set(ByVal value As Options)
            _QuestionOptions = value
        End Set
    End Property

    Public Overrides Function ToString() As String
        Return Me.Question
    End Function

End Class



'Quiz Class
'represents a single Quiz session
Public Class Quiz
    Private _NoOfQuestions As Integer   'no of questions to be asked
    Private _FilePath As String         'path of quiz.bin where all the questions are stored
    Private _EndOfQuiz As Boolean       'checks end of quiz
    Private _QuestionsLoaded As Boolean 'checks if questions were successfully loaded
    Private _Score As Long              'stores score of current quiz session
    Private _QuestionsCount As Long     'number of all questions retrieved from the file
    Private Count As Integer            'counts number of questions asked
    Private Questions As New ArrayList  'this arraylist object stores all the question objects
    Private DoneQuestions(0) As Integer 'stores already asked questions indices
    Private _SessionTimeOut As Long     'use this property to time a quiz session
    '                                    in seconds. set it to zero for infinite time
    Private WithEvents _tmr As System.Timers.Timer
    '                                    used by the class for timing
    Public Event Timeout()              'custom TimeOut event


    'constructor:
    'if no filepath is specified for file: (quiz.bin) while declaring
    'the quiz object it returns the default location of the file
    'ie: the application's root directory
    Sub New()
        MyBase.New()
        'initialize QuestionCount property to the size of questions
        'arraylist which holds all the questions
        _QuestionsCount = Questions.Count
        _tmr = New System.Timers.Timer
        If Trim(_FilePath) = "" Then
            _FilePath = My.Computer.FileSystem.CurrentDirectory & "\Quiz.bin"
        End If
    End Sub

    'oveloaded constructor:
    'the programmer can specify the total number of questions to be asked
    'in the quiz session while declaring the quiz object
    Sub New(ByVal lNoOfQuestions As Long)
        MyBase.New()
        _QuestionsCount = Questions.Count
        _tmr = New System.Timers.Timer
        If Trim(_FilePath) = "" Then
            _FilePath = My.Computer.FileSystem.CurrentDirectory & "\Quiz.bin"
        End If
        _NoOfQuestions = lNoOfQuestions
    End Sub
    Property SessionTimeOut() As Long
        Get
            SessionTimeOut = _SessionTimeOut
        End Get
        Set(ByVal value As Long)
            If value >= 0 Then
                _SessionTimeOut = value

                If value = 0 Then
                    _tmr.Enabled = False   'if set to 0 turn the timer off
                Else
                    _tmr.Interval = _SessionTimeOut * 1000  'convert to milliseconds
                    _tmr.Enabled = True
                End If

            Else
                Throw New Exception("cannot accept negative number")
            End If
        End Set
    End Property

    'returns the total number of questions retrieved from
    'the file
    ReadOnly Property QuestionsCount() As Long
        Get
            _QuestionsCount = Questions.Count
            Return _QuestionsCount
        End Get
    End Property

    'Starts a new quiz session
    Sub Restart()
        _EndOfQuiz = False
        _Score = 0             'set score to 0
        ReDim DoneQuestions(0) 'clear array
        Count = 0              'set count to 0
        If _SessionTimeOut > 0 Then
            _tmr.Interval = _SessionTimeOut * 1000  'convert to milliseconds
            _tmr.Enabled = True
        End If
    End Sub

    'returns the total score of a quiz session
    ReadOnly Property Score()
        Get
            Return _Score
        End Get
    End Property

    'adds a specific value to total score of a quiz session
    Sub AddToScore(ByVal value As Long)
        _Score = _Score + value
    End Sub

    'subtract a specific value from score
    'in case you wish to penalize the user!!!
    Sub SubtractFromScore(ByVal value As Long)
        _Score = _Score - value
    End Sub

    'checks if current quiz session has ended.
    ReadOnly Property EndOfQuiz() As Boolean
        Get
            Return _EndOfQuiz
        End Get
    End Property

    'returns or sets the number of questions to be asked per each
    'Quiz session
    Property NoOfQuestions() As Integer
        Get
            NoOfQuestions = _NoOfQuestions
        End Get
        Set(ByVal value As Integer)

            If _QuestionsLoaded = True Then

                If value > Questions.Count Then
                    Throw New Exception("NoOfQuestions exceed total number of questions in the database")
                ElseIf value <= 0 Then
                    Throw New Exception("NoOfQuestion cannot be assigned a zero or negative value")
                Else
                    _NoOfQuestions = value
                End If

            Else
                Throw New Exception("Call LoadQuestion() method first to load the questions")
            End If

        End Set
    End Property

    'returns or sets the path of Quiz.bin file where all the questions
    'are stored.
    Property FilePath() As String
        Get
            FilePath = _FilePath
        End Get
        Set(ByVal value As String)
            _FilePath = value
        End Set
    End Property

    'loads all the questions from the Quiz.bin file located in the root
    'directory of the running instance
    Sub LoadQuestions()
        Dim readFile As FileStream
        Dim formaTTer As New BinaryFormatter
        'clean up the arraylist of any possible junk values
        Questions.Clear()

        Try
            readFile = File.OpenRead(FilePath)
            Questions = CType(formaTTer.Deserialize(readFile), ArrayList)

            If Questions.Count <= 0 Then
                MsgBox("No questions found in the database.", MsgBoxStyle.Critical, Me.GetType)
                _QuestionsLoaded = False
                _QuestionsCount = 0
                Exit Sub
            Else
                'if NoOfQuestions property is not set then ask all the
                'available questions
                If _NoOfQuestions = 0 Then _NoOfQuestions = Questions.Count
                ReDim DoneQuestions(0)
                _QuestionsLoaded = True
                Questions.TrimToSize()
                _QuestionsCount = Questions.Count

            End If

        Catch ex As IO.FileNotFoundException
            MsgBox(ex.Message)
            _EndOfQuiz = True

        Finally
            If Not readFile Is Nothing Then
                readFile.Close()
            End If
        End Try

    End Sub

    'INTERESTING FUNCTION:
    'returns a random question, uses the function AlreadyDone() to check
    'for already asked questions. if the randomly generated question has
    'already been asked during the current quiz session it goes back to
    'the beginning of the While loop and searches again until a
    'not-perviously-asked question is returned

    Overloads Function GetQuestion() As QuizQuestion
        Dim QQ As QuizQuestion

        If _QuestionsCount > 0 Then
            Dim QN As Long

            If Count >= (_NoOfQuestions - 1) Then
                _EndOfQuiz = True
                Count = 0
                _tmr.Enabled = False
            End If

            While Count < _NoOfQuestions
                Randomize()
                QN = CInt(Int((_QuestionsCount * Rnd()) + 1))
                If Not AlreadyDone(QN) Then
                    ReDim Preserve DoneQuestions(DoneQuestions.GetUpperBound(0) + 1)
                    DoneQuestions(DoneQuestions.GetUpperBound(0)) = QN
                    Count = Count + 1

                    QQ = CType(Questions(QN - 1), QuizQuestion)
                    Application.DoEvents()
                    Return QQ
                End If
            End While

        Else
            Throw New Exception("NoQuestionsLoaded")
        End If

    End Function

    'returns a specific question, searching by its index
    Overloads Function GetQuestion(ByVal Index As Integer) As QuizQuestion

        Dim QQ As QuizQuestion
        QQ = CType(Questions(Index), QuizQuestion)
        Return QQ

    End Function

    'used by the overloaded GetQuestion() function to check if the returned
    'question has already been asked
    Protected Function AlreadyDone(ByVal value As Integer) As Boolean

        Dim i As Integer
        For Each i In DoneQuestions
            If i = value Then
                Return True
            End If
        Next

    End Function

    'let's the programmer add his/her own questions to the quiz
    Sub AddQuestion(ByVal Question As QuizQuestion)

        Questions.Add(Question)

    End Sub

    'deletes a specified question
    Sub DeleteQuestion(ByVal Question As QuizQuestion)
        If Questions.Contains(Question) Then
            Questions.Remove(Question)
            Questions.TrimToSize()
            Me.SaveQuestions()
        Else
            Throw New Exception("The specified question could not be found.")
        End If
    End Sub

    'when you've added all your questions finally call this method
    'to save the newly added questions to the default file :quiz.bin
    'MAKE SURE TO FIRST call LoadQuestions() method to load existing questions
    'then AddQuestion() to add your questions, and then finally
    'this method
    Overloads Sub SaveQuestions()

        If Dir(_FilePath) <> "" Then
            Kill(_FilePath)
        End If
        Dim saveFile As FileStream
        saveFile = File.OpenWrite(_FilePath)
        saveFile.Seek(0, SeekOrigin.End)
        Dim formaTTer As New BinaryFormatter
        formaTTer.Serialize(saveFile, Questions)
        saveFile.Close()

    End Sub

    'this overloaded version of SaveQuestions let's you specify your
    'own path in case you wish to save your questions in a different
    'file rather than the default quiz.bin file.
    Overloads Sub SaveQuestions(ByVal Path As String)

        Dim saveFile As FileStream
        saveFile = File.OpenWrite(Path)
        saveFile.Seek(0, SeekOrigin.End)
        Dim formaTTer As New BinaryFormatter
        formaTTer.Serialize(saveFile, Questions)
        saveFile.Close()

    End Sub

    'If SessionTimeout property is set to greater than 0, this
    'sub triggers the Timout event of the quiz object if the specified
    'time has gone by, and ends the quiz
    Private Sub _tmr_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles _tmr.Elapsed
        If _EndOfQuiz = False Then
            _EndOfQuiz = True
            _tmr.Enabled = False
            RaiseEvent Timeout()
        End If
    End Sub

End Class




