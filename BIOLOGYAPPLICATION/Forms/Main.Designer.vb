<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Me.FormSkin1 = New BIOLOGYAPPLICATION.FormSkin()
        Me.MainContentFlowPanel = New System.Windows.Forms.FlowLayoutPanel()
        Me.TestLayoutPanel = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.FlatLabel5 = New BIOLOGYAPPLICATION.FlatLabel()
        Me.FlatLabel4 = New BIOLOGYAPPLICATION.FlatLabel()
        Me.FlatLabel3 = New BIOLOGYAPPLICATION.FlatLabel()
        Me.FlatLabel2 = New BIOLOGYAPPLICATION.FlatLabel()
        Me.FlatLabel1 = New BIOLOGYAPPLICATION.FlatLabel()
        Me.FlatButton2 = New BIOLOGYAPPLICATION.FlatButton()
        Me.FlatButton5 = New BIOLOGYAPPLICATION.FlatButton()
        Me.cmdVideo = New BIOLOGYAPPLICATION.FlatButton()
        Me.FlatButton3 = New BIOLOGYAPPLICATION.FlatButton()
        Me.btnStartLesson = New BIOLOGYAPPLICATION.FlatButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.FormSkin1.SuspendLayout()
        Me.MainContentFlowPanel.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'FormSkin1
        '
        Me.FormSkin1.BackColor = System.Drawing.Color.White
        Me.FormSkin1.BaseColor = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.FormSkin1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(53, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.FormSkin1.Controls.Add(Me.MainContentFlowPanel)
        Me.FormSkin1.Controls.Add(Me.Panel5)
        Me.FormSkin1.Controls.Add(Me.Panel4)
        Me.FormSkin1.Controls.Add(Me.GroupBox1)
        Me.FormSkin1.Controls.Add(Me.Panel1)
        Me.FormSkin1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FormSkin1.FlatColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FormSkin1.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.FormSkin1.HeaderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FormSkin1.HeaderMaximize = False
        Me.FormSkin1.Location = New System.Drawing.Point(0, 0)
        Me.FormSkin1.Name = "FormSkin1"
        Me.FormSkin1.Size = New System.Drawing.Size(1111, 635)
        Me.FormSkin1.TabIndex = 0
        Me.FormSkin1.Text = "Interactive Instructional Materials in Biology"
        '
        'MainContentFlowPanel
        '
        Me.MainContentFlowPanel.AutoScroll = True
        Me.MainContentFlowPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.MainContentFlowPanel.Controls.Add(Me.TestLayoutPanel)
        Me.MainContentFlowPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MainContentFlowPanel.Font = New System.Drawing.Font("Georgia", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MainContentFlowPanel.Location = New System.Drawing.Point(129, 190)
        Me.MainContentFlowPanel.Name = "MainContentFlowPanel"
        Me.MainContentFlowPanel.Size = New System.Drawing.Size(853, 445)
        Me.MainContentFlowPanel.TabIndex = 20
        Me.MainContentFlowPanel.TabStop = True
        '
        'TestLayoutPanel
        '
        Me.TestLayoutPanel.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TestLayoutPanel.AutoScroll = True
        Me.TestLayoutPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.TestLayoutPanel.Location = New System.Drawing.Point(3, 3)
        Me.TestLayoutPanel.Name = "TestLayoutPanel"
        Me.TestLayoutPanel.Size = New System.Drawing.Size(732, 0)
        Me.TestLayoutPanel.TabIndex = 18
        Me.TestLayoutPanel.Visible = False
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Transparent
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel5.Location = New System.Drawing.Point(982, 190)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(129, 445)
        Me.Panel5.TabIndex = 5
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel4.Location = New System.Drawing.Point(0, 190)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(129, 445)
        Me.Panel4.TabIndex = 4
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.FlatLabel5)
        Me.GroupBox1.Controls.Add(Me.FlatLabel4)
        Me.GroupBox1.Controls.Add(Me.FlatLabel3)
        Me.GroupBox1.Controls.Add(Me.FlatLabel2)
        Me.GroupBox1.Controls.Add(Me.FlatLabel1)
        Me.GroupBox1.Controls.Add(Me.FlatButton2)
        Me.GroupBox1.Controls.Add(Me.FlatButton5)
        Me.GroupBox1.Controls.Add(Me.cmdVideo)
        Me.GroupBox1.Controls.Add(Me.FlatButton3)
        Me.GroupBox1.Controls.Add(Me.btnStartLesson)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Location = New System.Drawing.Point(0, 51)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1111, 139)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        '
        'FlatLabel5
        '
        Me.FlatLabel5.AutoSize = True
        Me.FlatLabel5.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel5.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FlatLabel5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.FlatLabel5.Location = New System.Drawing.Point(706, 107)
        Me.FlatLabel5.Name = "FlatLabel5"
        Me.FlatLabel5.Size = New System.Drawing.Size(67, 19)
        Me.FlatLabel5.TabIndex = 11
        Me.FlatLabel5.Text = "GAMES"
        '
        'FlatLabel4
        '
        Me.FlatLabel4.AutoSize = True
        Me.FlatLabel4.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel4.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FlatLabel4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.FlatLabel4.Location = New System.Drawing.Point(462, 107)
        Me.FlatLabel4.Name = "FlatLabel4"
        Me.FlatLabel4.Size = New System.Drawing.Size(71, 19)
        Me.FlatLabel4.TabIndex = 10
        Me.FlatLabel4.Text = "VIDEOS"
        '
        'FlatLabel3
        '
        Me.FlatLabel3.AutoSize = True
        Me.FlatLabel3.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel3.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FlatLabel3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.FlatLabel3.Location = New System.Drawing.Point(210, 107)
        Me.FlatLabel3.Name = "FlatLabel3"
        Me.FlatLabel3.Size = New System.Drawing.Size(146, 19)
        Me.FlatLabel3.TabIndex = 9
        Me.FlatLabel3.Text = "ABOUT BIOLOGY"
        '
        'FlatLabel2
        '
        Me.FlatLabel2.AutoSize = True
        Me.FlatLabel2.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FlatLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.FlatLabel2.Location = New System.Drawing.Point(913, 107)
        Me.FlatLabel2.Name = "FlatLabel2"
        Me.FlatLabel2.Size = New System.Drawing.Size(92, 19)
        Me.FlatLabel2.TabIndex = 8
        Me.FlatLabel2.Text = "SETTINGS"
        '
        'FlatLabel1
        '
        Me.FlatLabel1.AutoSize = True
        Me.FlatLabel1.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FlatLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.FlatLabel1.Location = New System.Drawing.Point(26, 107)
        Me.FlatLabel1.Name = "FlatLabel1"
        Me.FlatLabel1.Size = New System.Drawing.Size(117, 19)
        Me.FlatLabel1.TabIndex = 7
        Me.FlatLabel1.Text = "ALL LESSONS"
        '
        'FlatButton2
        '
        Me.FlatButton2.BackColor = System.Drawing.Color.Transparent
        Me.FlatButton2.BackgroundImage = CType(resources.GetObject("FlatButton2.BackgroundImage"), System.Drawing.Image)
        Me.FlatButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.FlatButton2.BaseColor = System.Drawing.Color.Transparent
        Me.FlatButton2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatButton2.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FlatButton2.Location = New System.Drawing.Point(220, 22)
        Me.FlatButton2.Name = "FlatButton2"
        Me.FlatButton2.Rounded = True
        Me.FlatButton2.Size = New System.Drawing.Size(117, 74)
        Me.FlatButton2.TabIndex = 6
        Me.FlatButton2.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'FlatButton5
        '
        Me.FlatButton5.BackColor = System.Drawing.Color.Transparent
        Me.FlatButton5.BackgroundImage = CType(resources.GetObject("FlatButton5.BackgroundImage"), System.Drawing.Image)
        Me.FlatButton5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.FlatButton5.BaseColor = System.Drawing.Color.Transparent
        Me.FlatButton5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatButton5.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FlatButton5.Location = New System.Drawing.Point(898, 22)
        Me.FlatButton5.Name = "FlatButton5"
        Me.FlatButton5.Rounded = True
        Me.FlatButton5.Size = New System.Drawing.Size(117, 74)
        Me.FlatButton5.TabIndex = 3
        Me.FlatButton5.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'cmdVideo
        '
        Me.cmdVideo.BackColor = System.Drawing.Color.Transparent
        Me.cmdVideo.BackgroundImage = CType(resources.GetObject("cmdVideo.BackgroundImage"), System.Drawing.Image)
        Me.cmdVideo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.cmdVideo.BaseColor = System.Drawing.Color.Transparent
        Me.cmdVideo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdVideo.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdVideo.Location = New System.Drawing.Point(441, 22)
        Me.cmdVideo.Name = "cmdVideo"
        Me.cmdVideo.Rounded = True
        Me.cmdVideo.Size = New System.Drawing.Size(117, 74)
        Me.cmdVideo.TabIndex = 2
        Me.cmdVideo.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'FlatButton3
        '
        Me.FlatButton3.BackColor = System.Drawing.Color.Transparent
        Me.FlatButton3.BackgroundImage = CType(resources.GetObject("FlatButton3.BackgroundImage"), System.Drawing.Image)
        Me.FlatButton3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.FlatButton3.BaseColor = System.Drawing.Color.Transparent
        Me.FlatButton3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatButton3.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FlatButton3.Location = New System.Drawing.Point(675, 22)
        Me.FlatButton3.Name = "FlatButton3"
        Me.FlatButton3.Rounded = True
        Me.FlatButton3.Size = New System.Drawing.Size(117, 74)
        Me.FlatButton3.TabIndex = 1
        Me.FlatButton3.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'btnStartLesson
        '
        Me.btnStartLesson.BackColor = System.Drawing.Color.Transparent
        Me.btnStartLesson.BackgroundImage = CType(resources.GetObject("btnStartLesson.BackgroundImage"), System.Drawing.Image)
        Me.btnStartLesson.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnStartLesson.BaseColor = System.Drawing.Color.Transparent
        Me.btnStartLesson.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnStartLesson.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStartLesson.Location = New System.Drawing.Point(21, 22)
        Me.btnStartLesson.Name = "btnStartLesson"
        Me.btnStartLesson.Rounded = True
        Me.btnStartLesson.Size = New System.Drawing.Size(117, 74)
        Me.btnStartLesson.TabIndex = 0
        Me.btnStartLesson.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1111, 51)
        Me.Panel1.TabIndex = 1
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Button2)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel2.Location = New System.Drawing.Point(909, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(58, 51)
        Me.Panel2.TabIndex = 1
        '
        'Button2
        '
        Me.Button2.BackgroundImage = CType(resources.GetObject("Button2.BackgroundImage"), System.Drawing.Image)
        Me.Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button2.FlatAppearance.BorderSize = 0
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Location = New System.Drawing.Point(3, 3)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(44, 45)
        Me.Button2.TabIndex = 0
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Button1)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel3.Location = New System.Drawing.Point(967, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(144, 51)
        Me.Panel3.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.BackgroundImage = CType(resources.GetObject("Button1.BackgroundImage"), System.Drawing.Image)
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Location = New System.Drawing.Point(3, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(137, 45)
        Me.Button1.TabIndex = 0
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1111, 635)
        Me.Controls.Add(Me.FormSkin1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Main"
        Me.TransparencyKey = System.Drawing.Color.Fuchsia
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.FormSkin1.ResumeLayout(False)
        Me.MainContentFlowPanel.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents FormSkin1 As BIOLOGYAPPLICATION.FormSkin
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnStartLesson As BIOLOGYAPPLICATION.FlatButton
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents FlatButton5 As BIOLOGYAPPLICATION.FlatButton
    Friend WithEvents cmdVideo As BIOLOGYAPPLICATION.FlatButton
    Friend WithEvents FlatButton3 As BIOLOGYAPPLICATION.FlatButton
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents FlatButton2 As BIOLOGYAPPLICATION.FlatButton
    Friend WithEvents FlatLabel5 As BIOLOGYAPPLICATION.FlatLabel
    Friend WithEvents FlatLabel4 As BIOLOGYAPPLICATION.FlatLabel
    Friend WithEvents FlatLabel3 As BIOLOGYAPPLICATION.FlatLabel
    Friend WithEvents FlatLabel2 As BIOLOGYAPPLICATION.FlatLabel
    Friend WithEvents FlatLabel1 As BIOLOGYAPPLICATION.FlatLabel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Public WithEvents MainContentFlowPanel As System.Windows.Forms.FlowLayoutPanel
    Public WithEvents TestLayoutPanel As System.Windows.Forms.FlowLayoutPanel
End Class
