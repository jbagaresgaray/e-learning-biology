Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.IO
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Windows.Forms

Imports BIOLOGYAPPLICATION.USERCONTROLS

Public Class Main
    Private Topics As New List(Of LessonInfo)()
    Public LessonId As String = String.Empty
    Private userRes As New UserResults()
    Private applicationUser As New User()
    Public CountOfCAnswers As String
    Public CountOfWAnswers As String

    Private paneWith As Integer

    Private Sub HideMainContent_Handler(sender As Object, e As EventArgs)
        Try
            MainContentFlowPanel.Visible = False
            TestLayoutPanel.Visible = True

            Dim CurrentTestInstruction As New UCTestInstruction()

            Dim panel As New Panel()
            panel.Width = Me.ClientSize.Width - 285
            panel.Height = Me.ClientSize.Height - 180
            panel.BorderStyle = BorderStyle.None

            CurrentTestInstruction.Dock = DockStyle.Fill

            Dim instruction = String.Empty
            If Me.LessonId <> String.Empty Then
                instruction = GetString("SELECT [instructions] FROM tbl_lessons WHERE lessonid =" & Me.LessonId)
                CurrentTestInstruction.txtInstruction.Text = instruction.ToString()
            End If

            AddHandler CurrentTestInstruction.StartTest, New EventHandler(AddressOf Me.Start_Handler)
            AddHandler CurrentTestInstruction.ExitTest, New EventHandler(AddressOf Me.ExitTest_Handler)

            panel.Controls.Add(CurrentTestInstruction)
            TestLayoutPanel.Controls.Add(panel)
        Catch generatedExceptionName As Exception
        End Try

    End Sub

    Private Sub Start_Handler(sender As Object, e As EventArgs)
        Try
            Dim questionCount As New List(Of QuestionsInfo)
            GetQuestions(Me.LessonId, questionCount)
            If questionCount.Count > 0 Then
                MainContentFlowPanel.Visible = False
                TestLayoutPanel.Visible = True
                TestLayoutPanel.Controls.Clear()
                Dim UserControlTest As New Test()
                UserControlTest.txtLessonId.Text = Me.LessonId.ToString()
                AddHandler UserControlTest.ExitTest, New EventHandler(AddressOf Me.ExitTest_Handler)
                AddHandler UserControlTest.ShowFinalResult, New EventHandler(AddressOf Me.FinalResult_Handler)
                'AddHandler UserControlTest.collectResults, New CollectResults(AddressOf Me.GetResults_Handler)
                TestLayoutPanel.Controls.Add(UserControlTest)
            Else
            System.Windows.Forms.MessageBox.Show("There are no questions on this topic", "Information", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information)
            End If
        Catch generatedExceptionName As Exception
        End Try

    End Sub

    Private Sub GetResults_Handler(results As UserResults)
        Try
            userRes = results

            If results.correctAnswers Is Nothing Then
                Me.CountOfCAnswers = Nothing
            Else
                Me.CountOfCAnswers = results.correctAnswers.Length.ToString()
            End If
            If results.wrongAnswers Is Nothing Then
                Me.CountOfWAnswers = "0"
            Else
                Me.CountOfWAnswers = results.wrongAnswers.Length.ToString()
            End If

            Dim sb As New StringBuilder()
            Dim arr As Char() = New Char() {ControlChars.Cr, ControlChars.Lf, " "c}
            Dim countCorrect As Integer = 1
            Dim countWrong As Integer = 1
            sb.Append("Correct Answers " & vbCr & vbLf)
            sb.AppendLine("--------------------------------")
            If userRes.correctAnswers IsNot Nothing Then
                For Each str As String In userRes.correctAnswers
                    sb.Append(System.Math.Max(System.Threading.Interlocked.Increment(countCorrect), countCorrect - 1) & "." & str.ToString().TrimStart(arr).TrimEnd(arr) & vbCr & vbLf)
                Next
            Else
                sb.Append(" 0 " & vbCr & vbLf)
            End If

            sb.Append(vbCr & vbLf)
            sb.Append("Wrong Answers " & vbCr & vbLf)
            sb.AppendLine("--------------------------------")
            If userRes.wrongAnswers IsNot Nothing Then
                For Each str As String In userRes.wrongAnswers
                    sb.Append(System.Math.Max(System.Threading.Interlocked.Increment(countWrong), countWrong - 1) & "." & str.ToString().TrimStart(arr).TrimEnd(arr) & vbCr & vbLf)
                Next
            Else
                sb.Append("0 " & vbCr & vbLf)
            End If

            sb.Append(vbCr & vbLf)
            sb.Append("Your Test Result is: ")
            sb.Append("" & Me.CountOfCAnswers & "/" & (Convert.ToInt32(Me.CountOfWAnswers) + Convert.ToInt32(Me.CountOfCAnswers)) & "  " & vbCr & vbLf)
            sb.Append(vbCr & vbLf)

            userRes.result = sb.ToString()
            userRes.userId = Me.applicationUser.userId
            CollectUserResults(userRes)
            Dim testResult As New TestResults()
            testResult.studentName = applicationUser.username.ToString()
            testResult.result = Me.CountOfCAnswers & "/" & (Convert.ToInt32(Me.CountOfWAnswers) + Convert.ToInt32(Me.CountOfCAnswers))
            testResult.createdate = DateTime.Now.ToShortDateString()

            InsertResultsToDatabse(testResult)
        Catch generatedExceptionName As Exception
        End Try

    End Sub

    Private Sub FinalResult_Handler(sender As Object, e As EventArgs)
        Try
            Dim finalTestResult As New UCTestResult()
            TestLayoutPanel.Controls.Clear()
            AddHandler finalTestResult.GoToLessons, New EventHandler(AddressOf Me.ExitTest_Handler)

            If Me.CountOfWAnswers Is Nothing Then
                finalTestResult.txtCorrectAnswer.Text = "0"
            Else
                finalTestResult.txtCorrectAnswer.Text = Me.CountOfCAnswers.ToString()
            End If

            If Me.CountOfWAnswers Is Nothing Then
                finalTestResult.txtWrongAnswer.Text = "0"
            Else
                finalTestResult.txtWrongAnswer.Text = Me.CountOfWAnswers.ToString()
            End If
            finalTestResult.Width = Me.ClientSize.Width - 380
            finalTestResult.Height = Me.ClientSize.Height - 380
            finalTestResult.RTResults.Text = userRes.result.ToString()
            finalTestResult.lblResults.Text = Me.CountOfCAnswers.ToString() & "/" & Convert.ToString(Convert.ToInt32(Me.CountOfCAnswers.ToString()) + Convert.ToInt32(Me.CountOfWAnswers.ToString()))
            CollectUserResults(userRes)
            TestLayoutPanel.Controls.Add(finalTestResult)
        Catch generatedExceptionName As Exception
        End Try
    End Sub

    Private Sub ExitTest_Handler(sender As Object, e As EventArgs)
        TestLayoutPanel.Controls.Clear()
        TestLayoutPanel.Visible = False
        MainContentFlowPanel.Visible = True
        btnStartLesson_Click(sender, e)
    End Sub

    Private Sub ExposeCurrentlySelectedTestId_Handler()
        MessageBox.Show("Succesfully Deleted !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Public Function PassCurrentLessonIdFromMain(currentLessonId As String) As String
        Me.LessonId = currentLessonId
        Return Me.LessonId
    End Function

    Private Sub Main_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try

            TestLayoutPanel.Visible = False
            Dim maincontent As New UCMainContent()
            MainContentFlowPanel.Controls.Clear()

            maincontent.Width = MainContentFlowPanel.Width
            maincontent.Height = MainContentFlowPanel.Height

            MainContentFlowPanel.Controls.Add(maincontent)

            paneWith = MainContentFlowPanel.Width

        Catch generatedExceptionName As Exception
        End Try
    End Sub

    Private Sub Main_Resize(sender As System.Object, e As System.EventArgs) Handles MyBase.Resize
        GroupBox1.Left = (Me.Width - GroupBox1.Width) / 2
        GroupBox1.Top = (Me.Height - GroupBox1.Height) / 2
    End Sub

    Private Sub cmdVideo_Click(sender As System.Object, e As System.EventArgs) Handles cmdVideo.Click
        Dim frm As New frmVideos
        frm.WindowState = FormWindowState.Maximized
        frm.ShowDialog()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Application.Exit()
    End Sub

    Private Sub FlatButton5_Click(sender As Object, e As EventArgs) Handles FlatButton5.Click
        Dim frm As New frmSettings
        frm.ShowDialog()
    End Sub

    Private Sub FlatButton2_Click(sender As Object, e As EventArgs) Handles FlatButton2.Click
        Dim frm As New frmAbout
        frm.WindowState = FormWindowState.Maximized
        frm.ShowDialog()
    End Sub

    Private Sub btnStartLesson_Click(sender As Object, e As EventArgs) Handles btnStartLesson.Click
        Dim frm As New frmLessons
        frm.WindowState = FormWindowState.Maximized
        frm.ShowDialog()
    End Sub

End Class
