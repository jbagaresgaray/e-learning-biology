Namespace FORMS
    Partial Class frmLessonAE
        ''' <summary>
        ''' Required designer variable.
        ''' </summary>
        Private components As System.ComponentModel.IContainer = Nothing

        ''' <summary>
        ''' Clean up any resources being used.
        ''' </summary>
        ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        Protected Overrides Sub Dispose(disposing As Boolean)
            If disposing AndAlso (components IsNot Nothing) Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

#Region "Windows Form Designer generated code"

        ''' <summary>
        ''' Required method for Designer support - do not modify
        ''' the contents of this method with the code editor.
        ''' </summary>
        Private Sub InitializeComponent()
            Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(frmLessonAE))
            Me.txtLessonTitle = New System.Windows.Forms.TextBox()
            Me.btnSaveUpdate = New System.Windows.Forms.Button()
            Me.btnCancel = New System.Windows.Forms.Button()
            Me.lblLesson = New System.Windows.Forms.Label()
            Me.txtLessonDesc = New System.Windows.Forms.TextBox()
            Me.label1 = New System.Windows.Forms.Label()
            Me.label2 = New System.Windows.Forms.Label()
            Me.txtInstructions = New System.Windows.Forms.TextBox()
            Me.openFileDialog1 = New System.Windows.Forms.OpenFileDialog()
            Me.btnBrowse = New System.Windows.Forms.Button()
            Me.pic = New System.Windows.Forms.PictureBox()
            DirectCast(Me.pic, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            ' txtLessonTitle
            '
            Me.txtLessonTitle.Font = New System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
            Me.txtLessonTitle.Location = New System.Drawing.Point(99, 11)
            Me.txtLessonTitle.Multiline = True
            Me.txtLessonTitle.Name = "txtLessonTitle"
            Me.txtLessonTitle.Size = New System.Drawing.Size(426, 27)
            Me.txtLessonTitle.TabIndex = 1
            '
            ' btnSaveUpdate
            '
            Me.btnSaveUpdate.Location = New System.Drawing.Point(531, 291)
            Me.btnSaveUpdate.Name = "btnSaveUpdate"
            Me.btnSaveUpdate.Size = New System.Drawing.Size(83, 27)
            Me.btnSaveUpdate.TabIndex = 3
            Me.btnSaveUpdate.Text = "Save"
            Me.btnSaveUpdate.UseVisualStyleBackColor = True
            AddHandler Me.btnSaveUpdate.Click, New System.EventHandler(AddressOf Me.btnSaveUpdate_Click)
            '
            ' btnCancel
            '
            Me.btnCancel.Location = New System.Drawing.Point(614, 291)
            Me.btnCancel.Name = "btnCancel"
            Me.btnCancel.Size = New System.Drawing.Size(83, 27)
            Me.btnCancel.TabIndex = 4
            Me.btnCancel.Text = "Cancel"
            Me.btnCancel.UseVisualStyleBackColor = True
            AddHandler Me.btnCancel.Click, New System.EventHandler(AddressOf Me.btnCancel_Click)
            '
            ' lblLesson
            '
            Me.lblLesson.AutoSize = True
            Me.lblLesson.Font = New System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
            Me.lblLesson.Location = New System.Drawing.Point(17, 16)
            Me.lblLesson.Name = "lblLesson"
            Me.lblLesson.Size = New System.Drawing.Size(81, 16)
            Me.lblLesson.TabIndex = 87
            Me.lblLesson.Text = "Lesson Title:"
            '
            ' txtLessonDesc
            '
            Me.txtLessonDesc.Font = New System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
            Me.txtLessonDesc.Location = New System.Drawing.Point(99, 44)
            Me.txtLessonDesc.Multiline = True
            Me.txtLessonDesc.Name = "txtLessonDesc"
            Me.txtLessonDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.txtLessonDesc.Size = New System.Drawing.Size(426, 115)
            Me.txtLessonDesc.TabIndex = 2
            '
            ' label1
            '
            Me.label1.AutoSize = True
            Me.label1.Font = New System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
            Me.label1.Location = New System.Drawing.Point(21, 44)
            Me.label1.Name = "label1"
            Me.label1.Size = New System.Drawing.Size(76, 16)
            Me.label1.TabIndex = 89
            Me.label1.Text = "Description:"
            '
            ' label2
            '
            Me.label2.AutoSize = True
            Me.label2.Font = New System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
            Me.label2.Location = New System.Drawing.Point(19, 165)
            Me.label2.Name = "label2"
            Me.label2.Size = New System.Drawing.Size(79, 16)
            Me.label2.TabIndex = 91
            Me.label2.Text = "Instructions:"
            '
            ' txtInstructions
            '
            Me.txtInstructions.Font = New System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
            Me.txtInstructions.Location = New System.Drawing.Point(99, 165)
            Me.txtInstructions.Multiline = True
            Me.txtInstructions.Name = "txtInstructions"
            Me.txtInstructions.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.txtInstructions.Size = New System.Drawing.Size(598, 115)
            Me.txtInstructions.TabIndex = 92
            '
            ' openFileDialog1
            '
            Me.openFileDialog1.FileName = "openFileDialog1"
            '
            ' btnBrowse
            '
            Me.btnBrowse.Location = New System.Drawing.Point(654, 131)
            Me.btnBrowse.Name = "btnBrowse"
            Me.btnBrowse.Size = New System.Drawing.Size(36, 23)
            Me.btnBrowse.TabIndex = 96
            Me.btnBrowse.Text = "..."
            Me.btnBrowse.UseVisualStyleBackColor = True
            AddHandler Me.btnBrowse.Click, New System.EventHandler(AddressOf Me.btnBrowse_Click)
            '
            ' pic
            '
            Me.pic.BackgroundImage = DirectCast(resources.GetObject("pic.BackgroundImage"), System.Drawing.Image)
            Me.pic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
            Me.pic.Location = New System.Drawing.Point(531, 12)
            Me.pic.Name = "pic"
            Me.pic.Size = New System.Drawing.Size(166, 148)
            Me.pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
            Me.pic.TabIndex = 95
            Me.pic.TabStop = False
            '
            ' AELesson
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0F, 16.0F)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(720, 333)
            Me.Controls.Add(Me.btnBrowse)
            Me.Controls.Add(Me.pic)
            Me.Controls.Add(Me.txtInstructions)
            Me.Controls.Add(Me.label2)
            Me.Controls.Add(Me.txtLessonDesc)
            Me.Controls.Add(Me.label1)
            Me.Controls.Add(Me.txtLessonTitle)
            Me.Controls.Add(Me.btnSaveUpdate)
            Me.Controls.Add(Me.btnCancel)
            Me.Controls.Add(Me.lblLesson)
            Me.Font = New System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
            Me.KeyPreview = True
            Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "AELesson"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
            Me.TopMost = True
            AddHandler Me.Load, New System.EventHandler(AddressOf Me.AELesson_Load)
            AddHandler Me.KeyPress, New System.Windows.Forms.KeyPressEventHandler(AddressOf Me.AddEditLesson_KeyPress)
            DirectCast(Me.pic, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        Private txtLessonTitle As System.Windows.Forms.TextBox
        Public btnSaveUpdate As System.Windows.Forms.Button
        Private btnCancel As System.Windows.Forms.Button
        Private lblLesson As System.Windows.Forms.Label
        Private txtLessonDesc As System.Windows.Forms.TextBox
        Private label1 As System.Windows.Forms.Label
        Private label2 As System.Windows.Forms.Label
        Private txtInstructions As System.Windows.Forms.TextBox
        Private openFileDialog1 As System.Windows.Forms.OpenFileDialog
        Private btnBrowse As System.Windows.Forms.Button
        Private pic As System.Windows.Forms.PictureBox

    End Class
End Namespace
