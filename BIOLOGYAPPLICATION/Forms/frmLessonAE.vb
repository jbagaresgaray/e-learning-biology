Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Windows.Forms
Imports BIOLOGYAPPLICATION.IIM.Business
Imports BIOLOGYAPPLICATION.IIM.Entities
Imports BIOLOGYAPPLICATION.RSFLib

Namespace FORMS
    Partial Public Class frmLessonAE
        Inherits Form
        Public Event RefreshListView As EventHandler

        Public Property Id() As String
            Get
                Return m_Id
            End Get
            Set(value As String)
                m_Id = value
            End Set
        End Property
        Private m_Id As String

        Public Sub New()
            InitializeComponent()
        End Sub

        Private Sub AddEditLesson_KeyPress(sender As Object, e As KeyPressEventArgs)

        End Sub

        Private Sub btnAdd_Click(sender As Object, e As EventArgs)
            Dim addditquestion As New frmQuestionAE()
            addditquestion.Show(Me)
        End Sub

        Private Sub btnCancel_Click(sender As Object, e As EventArgs)
            Me.Close()
        End Sub

        Private Sub btnSaveUpdate_Click(sender As Object, e As EventArgs)
            Dim l As New LessonInfo()
            l.lessonId = Convert.ToInt32(Me.Id)
            l.lessonTitle = txtLessonTitle.Text.Replace("'", " ")
            l.lessonDescription = txtLessonDesc.Text.Replace("'", " ")
            l.instructions = txtInstructions.Text.Replace("'", " ")
            If pic.Image Is Nothing Then
                Dim imgPath = AppDomain.CurrentDomain.BaseDirectory & "thumbnail-default.jpg"
                pic.Image = Image.FromFile(imgPath)
            End If
            Dim stream As New MemoryStream()
            pic.Image.Save(stream, ImageFormat.Jpeg)
            Dim photo_aray As Byte() = New Byte(stream.Length - 1) {}
            stream.Position = 0
            stream.Read(photo_aray, 0, photo_aray.Length)

            l.photo = photo_aray.ToArray()

            If btnSaveUpdate.Text = "Update" Then
                l.lessonId = Convert.ToInt32(Me.Id)
                If UpdateLesson(l) = True Then
                    PromptsMessage(2)
                    Me.Close()
                End If
            Else
                If CreateNewLesson(l) = True Then
                    PromptsMessage(1)
                    Me.Close()
                End If
            End If

            RaiseEvent RefreshListView(sender, e)
        End Sub

        Private Sub AELesson_Load(sender As Object, e As EventArgs)
            If btnSaveUpdate.Text = "Update" Then
                Dim lesson As New LessonInfo()
                If LessonDetails(Me.Id.ToString(), lesson) Then
                    txtLessonTitle.Text = lesson.lessontitle.Replace("'", " ")
                    txtLessonDesc.Text = lesson.lessondescription.Replace("'", " ")
                    txtInstructions.Text = lesson.instructions.Replace("'", " ")
                    Try
                        pic.Image = Nothing
                        Dim photoArray As Byte() = lesson.photo
                        Dim ms As New MemoryStream(photoArray)
                        ms.Write(photoArray, 0, photoArray.Length)
                        ms.Seek(0, SeekOrigin.Begin)

                        pic.Image = Image.FromStream(ms)
                    Catch generatedExceptionName As Exception
                    End Try
                End If
            End If
        End Sub

        Private Sub btnBrowse_Click(sender As Object, e As EventArgs)
            Dim res As DialogResult = openFileDialog1.ShowDialog()
            If res = DialogResult.OK Then
                pic.Image = Image.FromFile(openFileDialog1.FileName)
            End If
        End Sub
    End Class
End Namespace
