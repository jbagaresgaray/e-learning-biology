Imports BIOLOGYAPPLICATION.IIM.Entities
Imports BIOLOGYAPPLICATION.USERCONTROLS
Imports System.IO

Public Class frmLessons

    Private Topics As New List(Of LessonInfo)()
    Public LessonId As String = String.Empty
    Private userRes As New UserResults()
    Private applicationUser As New User()
    Public CountOfCAnswers As String
    Public CountOfWAnswers As String

    Private paneWith As Integer


    Private Sub HideMainContent_Handler(sender As Object, e As EventArgs)
        Try
            MainContentFlowPanel.Visible = False
            TestLayoutPanel.Visible = True

            Dim CurrentTestInstruction As New UCTestInstruction()

            Dim panel As New Panel()
            panel.Width = Me.ClientSize.Width - 285
            panel.Height = Me.ClientSize.Height - 180
            panel.BorderStyle = BorderStyle.None

            CurrentTestInstruction.Dock = DockStyle.Fill

            Dim instruction = String.Empty
            If Me.LessonId <> String.Empty Then
                instruction = GetString("SELECT [instructions] FROM tbl_lessons WHERE lessonid =" & Me.LessonId)
                CurrentTestInstruction.txtInstruction.Text = instruction.ToString()
            End If

            panel.Controls.Add(CurrentTestInstruction)
            TestLayoutPanel.Controls.Add(panel)
        Catch generatedExceptionName As Exception
        End Try

    End Sub

    Public Function PassCurrentLessonIdFromMain(currentLessonId As String) As String
        Me.LessonId = currentLessonId
        Return Me.LessonId
    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub frmLessons_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            If CURRENTUSER.uType = 1 Then
                Topics = GetAdminLessons()
            Else
                Topics = GetUserLessons(CURRENTUSER.IDUSER)
            End If

            MainContentFlowPanel.Controls.Clear()
            For Each topic As LessonInfo In Topics
                Dim panel As New Panel()
                panel.Height = 250
                panel.Width = Me.ClientSize.Width - 330
                panel.BorderStyle = BorderStyle.None
                Dim ucTopic As New UCTopics()
                ucTopic.Height = 253
                ucTopic.txtLessonId.Text = topic.lessonID.ToString()
                ucTopic.txtTitle.Text = topic.lessontitle.ToString()
                ucTopic.Width = Me.ClientSize.Width - 330
                Try
                    ucTopic.photo.Image = Nothing
                    Dim photoArray As Byte() = topic.photo
                    Dim ms As New MemoryStream(photoArray)
                    ms.Write(photoArray, 0, photoArray.Length)
                    ms.Seek(0, SeekOrigin.Begin)
                    ucTopic.photo.Image = Image.FromStream(ms)
                Catch generatedExceptionName As Exception
                End Try
                ucTopic.txtDescription.Text = topic.lessondescription.ToString()
                AddHandler ucTopic.HideAllLessonContent, New EventHandler(AddressOf Me.HideMainContent_Handler)
                ' pass a handler
                ucTopic.passCurrentLessonIdToMain = DirectCast([Delegate].Combine(ucTopic.passCurrentLessonIdToMain, New PassCurrentLessonIdToMain(AddressOf Me.PassCurrentLessonIdFromMain)), PassCurrentLessonIdToMain)
                panel.Controls.Add(ucTopic)
                MainContentFlowPanel.Controls.Add(panel)
            Next
        Catch generatedExceptionName As Exception
        End Try
    End Sub
End Class
