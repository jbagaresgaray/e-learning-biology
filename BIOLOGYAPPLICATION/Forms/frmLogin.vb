Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Data.OleDb
Imports System.Windows.Forms

Public Class frmLogin

    Dim Username, Password As String
    Dim sValid As Boolean = False


    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim key As String = ""

        Dim attempts As Integer
        attempts = 0

        Username = txtUName.Text
        Password = txtPwd.Text

        If Login(txtUName.Text, txtPwd.Text) Then
            Close()
        End If

        Dim verify As New cls_login
        If verify.login(Username, Password) = True Then
            attempts = 0
            Exit Sub
            frmMain.Show()
        Else
            txtUName.Focus()
            txtPwd.Text = ""
            attempts += 1

            If attempts = 3 Then
                MessageBox.Show("Oops! I think you are trying to LogIn an invalid USER ACCOUNT. Please check the information", "AFC NONACE System", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.Close()
            End If
        End If
    End Sub

    Private Function Login(ByVal Username As String, ByVal Password As String) As Boolean
        Dim con As New OleDbConnection(DB_CONNECTION_STRING)
        con.Open()
        Try

            Dim sLogin As String = "SELECT * FROM tbl_users WHERE username = '" & Me.txtUName.Text & "' AND password = '" & Me.txtPwd.Text & "'"
            Dim com As New OleDbCommand(sLogin, con)
            Dim dr As OleDbDataReader = com.ExecuteReader
            dr.Read()
            If dr.HasRows Then

                sValid = True

                CURRENTUSER.IDUSER = dr("userid").ToString
                CURRENTUSER.USERNAME = dr("username").ToString
                CURRENTUSER.uType = dr("userlevel").ToString
                isLogin = True

                Login = True
            Else
                isLogin = False
                Login = False
                MessageBox.Show("Invalid Password or Username", "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

            dr.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Interactive Instructional Material", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Login = False
        End Try
        con.Close()
    End Function

    Public Class cls_login
        Public Function login(ByVal Username As String, ByVal Password As String) As Boolean
            If Username = "' & Username & '" And Password = "' & Password & '" Then
                Return True
                frmMain.Show()
            Else
                Return False
            End If
        End Function
    End Class

    Private Sub frmLogin_Resize(sender As System.Object, e As System.EventArgs) Handles MyBase.Resize
        Panel1.Left = (Me.Width - Panel1.Width) / 2
        Panel1.Top = (Me.Height - Panel1.Height) / 2
    End Sub

    Private Sub txtPwd_KeyDown(sender As Object, e As KeyEventArgs) Handles txtPwd.KeyDown
        If (e.KeyCode And Not e.Modifiers) = Keys.Enter Then
            btnLogin_Click(Me, New System.EventArgs)
        End If
    End Sub
End Class
