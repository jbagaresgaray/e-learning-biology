Public Class frmMain

    Private Sub LogINToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LogINToolStripMenuItem.Click
        Dim login As New frmLogin
        login.ShowDialog()
    End Sub

    Private Sub SettingsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SettingsToolStripMenuItem.Click
        Dim setting As New frmSettings
        setting.ShowDialog()
    End Sub

    Private Sub NewLessonToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewLessonToolStripMenuItem.Click
        Dim AddNewLesson As New frmNewLesson
        AddNewLesson.ShowDialog()
    End Sub
End Class
