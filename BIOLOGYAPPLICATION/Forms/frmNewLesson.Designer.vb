<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNewLesson
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNewLesson))
        Me.btnBrowse = New System.Windows.Forms.Button()
        Me.pic = New System.Windows.Forms.PictureBox()
        Me.txtInstructions = New System.Windows.Forms.TextBox()
        Me.label2 = New System.Windows.Forms.Label()
        Me.txtLessonDesc = New System.Windows.Forms.TextBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.txtLessonTitle = New System.Windows.Forms.TextBox()
        Me.btnSaveUpdate = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.lblLesson = New System.Windows.Forms.Label()
        CType(Me.pic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(648, 132)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(36, 23)
        Me.btnBrowse.TabIndex = 106
        Me.btnBrowse.Text = "..."
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'pic
        '
        Me.pic.BackgroundImage = CType(resources.GetObject("pic.BackgroundImage"), System.Drawing.Image)
        Me.pic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pic.Location = New System.Drawing.Point(525, 13)
        Me.pic.Name = "pic"
        Me.pic.Size = New System.Drawing.Size(166, 148)
        Me.pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic.TabIndex = 105
        Me.pic.TabStop = False
        '
        'txtInstructions
        '
        Me.txtInstructions.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInstructions.Location = New System.Drawing.Point(93, 166)
        Me.txtInstructions.Multiline = True
        Me.txtInstructions.Name = "txtInstructions"
        Me.txtInstructions.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtInstructions.Size = New System.Drawing.Size(598, 115)
        Me.txtInstructions.TabIndex = 104
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label2.Location = New System.Drawing.Point(13, 166)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(79, 16)
        Me.label2.TabIndex = 103
        Me.label2.Text = "Instructions:"
        '
        'txtLessonDesc
        '
        Me.txtLessonDesc.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLessonDesc.Location = New System.Drawing.Point(93, 45)
        Me.txtLessonDesc.Multiline = True
        Me.txtLessonDesc.Name = "txtLessonDesc"
        Me.txtLessonDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtLessonDesc.Size = New System.Drawing.Size(426, 115)
        Me.txtLessonDesc.TabIndex = 98
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label1.Location = New System.Drawing.Point(15, 45)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(76, 16)
        Me.label1.TabIndex = 102
        Me.label1.Text = "Description:"
        '
        'txtLessonTitle
        '
        Me.txtLessonTitle.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLessonTitle.Location = New System.Drawing.Point(93, 12)
        Me.txtLessonTitle.Multiline = True
        Me.txtLessonTitle.Name = "txtLessonTitle"
        Me.txtLessonTitle.Size = New System.Drawing.Size(426, 27)
        Me.txtLessonTitle.TabIndex = 97
        '
        'btnSaveUpdate
        '
        Me.btnSaveUpdate.Location = New System.Drawing.Point(525, 292)
        Me.btnSaveUpdate.Name = "btnSaveUpdate"
        Me.btnSaveUpdate.Size = New System.Drawing.Size(83, 27)
        Me.btnSaveUpdate.TabIndex = 99
        Me.btnSaveUpdate.Text = "Save"
        Me.btnSaveUpdate.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(608, 292)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(83, 27)
        Me.btnCancel.TabIndex = 100
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'lblLesson
        '
        Me.lblLesson.AutoSize = True
        Me.lblLesson.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLesson.Location = New System.Drawing.Point(11, 17)
        Me.lblLesson.Name = "lblLesson"
        Me.lblLesson.Size = New System.Drawing.Size(81, 16)
        Me.lblLesson.TabIndex = 101
        Me.lblLesson.Text = "Lesson Title:"
        '
        'frmNewLesson
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(701, 322)
        Me.Controls.Add(Me.btnBrowse)
        Me.Controls.Add(Me.pic)
        Me.Controls.Add(Me.txtInstructions)
        Me.Controls.Add(Me.label2)
        Me.Controls.Add(Me.txtLessonDesc)
        Me.Controls.Add(Me.label1)
        Me.Controls.Add(Me.txtLessonTitle)
        Me.Controls.Add(Me.btnSaveUpdate)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.lblLesson)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmNewLesson"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Adding New Lesson"
        CType(Me.pic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents btnBrowse As System.Windows.Forms.Button
    Private WithEvents pic As System.Windows.Forms.PictureBox
    Private WithEvents txtInstructions As System.Windows.Forms.TextBox
    Private WithEvents label2 As System.Windows.Forms.Label
    Private WithEvents txtLessonDesc As System.Windows.Forms.TextBox
    Private WithEvents label1 As System.Windows.Forms.Label
    Private WithEvents txtLessonTitle As System.Windows.Forms.TextBox
    Public WithEvents btnSaveUpdate As System.Windows.Forms.Button
    Private WithEvents btnCancel As System.Windows.Forms.Button
    Private WithEvents lblLesson As System.Windows.Forms.Label
End Class
