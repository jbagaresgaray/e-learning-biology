Imports System.Data.OleDb
Imports System.Data
Friend Class frmNewLesson

    Public State As FormState
    Dim Lesson As LessonInfo
    Dim ID As Integer

    Private Sub btnSaveUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveUpdate.Click
        Select Case State
            Case FormState.addState
                If ExecuteQuery("INSERT INTO tbl_lessons(lessontitle, lessondescription, createdate, userid, instructions) " _
                        & "VALUES('" & txtLessonTitle.Text & "', '" & txtLessonDesc.Text & "', '" & DateTime.Now.ToString("yyyy-MM-dd") & "', " _
                        & "" & CURRENTUSER.IDUSER & ",'" & txtInstructions.Text & "')") Then
                    MessageBox.Show("New lesson has been added.", "Interactive Instructional Materials in Biology")
                Else
                    MessageBox.Show("Error in adding new lesson", "Interactive Instructional Materials in Biology")
                End If

            Case FormState.updateState
                If ExecuteQuery("UPDATE tbl_lessons SET lessontitle='" & Trim(txtLessonTitle.Text) & "', lessondescription='" & Trim(txtLessonDesc.Text) & "', " _
                                & "modifydate='" & DateTime.Now.ToString("yyyy-MM-dd") & "', userid=" & CURRENTUSER.IDUSER & ", instructions='" & Trim(txtInstructions.Text) & "' " _
                                & "WHERE lessonid=" & ID & "") Then
                    MessageBox.Show("Lesson has been successfully updated.", "Interactive Instructional Materials in Biology")
                Else
                    MessageBox.Show("Error in updating lesson", "Interactive Instructional Materials in Biology")
                End If
        End Select

    End Sub

    Public Sub LessonDet(ByVal lessonID As Integer)

        If LessonDetails(lessonID, Lesson) Then
            ID = Lesson.lessonID
            txtLessonTitle.Text = Lesson.lessontitle
            txtLessonDesc.Text = Lesson.lessondescription
            txtInstructions.Text = Lesson.instructions
        Else
            Exit Sub
        End If

        State = FormState.updateState
        Me.ShowDialog()
    End Sub

    Public Sub ShowForm()
        State = FormState.addState
        Me.ShowDialog()
    End Sub
End Class
