<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNewQ
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSaveUpdate = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.lvList = New System.Windows.Forms.ListView()
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cboID = New System.Windows.Forms.ComboBox()
        Me.cboLessons = New System.Windows.Forms.ComboBox()
        Me.lblLesson = New System.Windows.Forms.Label()
        Me.lblAnswer1 = New System.Windows.Forms.Label()
        Me.txtAnswer1 = New System.Windows.Forms.TextBox()
        Me.txtQuestionText = New System.Windows.Forms.TextBox()
        Me.txtQuestion = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSaveUpdate
        '
        Me.btnSaveUpdate.Location = New System.Drawing.Point(380, 384)
        Me.btnSaveUpdate.Name = "btnSaveUpdate"
        Me.btnSaveUpdate.Size = New System.Drawing.Size(83, 27)
        Me.btnSaveUpdate.TabIndex = 113
        Me.btnSaveUpdate.Text = "Save"
        Me.btnSaveUpdate.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(464, 384)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(83, 27)
        Me.btnCancel.TabIndex = 115
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnAdd)
        Me.GroupBox1.Controls.Add(Me.lvList)
        Me.GroupBox1.Controls.Add(Me.cboID)
        Me.GroupBox1.Controls.Add(Me.cboLessons)
        Me.GroupBox1.Controls.Add(Me.lblLesson)
        Me.GroupBox1.Controls.Add(Me.lblAnswer1)
        Me.GroupBox1.Controls.Add(Me.txtAnswer1)
        Me.GroupBox1.Controls.Add(Me.txtQuestionText)
        Me.GroupBox1.Controls.Add(Me.txtQuestion)
        Me.GroupBox1.Location = New System.Drawing.Point(5, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(542, 375)
        Me.GroupBox1.TabIndex = 116
        Me.GroupBox1.TabStop = False
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(15, 167)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(83, 27)
        Me.btnAdd.TabIndex = 117
        Me.btnAdd.Text = "Add to LIST"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'lvList
        '
        Me.lvList.CheckBoxes = True
        Me.lvList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader2, Me.ColumnHeader1})
        Me.lvList.FullRowSelect = True
        Me.lvList.GridLines = True
        Me.lvList.Location = New System.Drawing.Point(100, 145)
        Me.lvList.Name = "lvList"
        Me.lvList.Size = New System.Drawing.Size(434, 224)
        Me.lvList.TabIndex = 141
        Me.lvList.UseCompatibleStateImageBehavior = False
        Me.lvList.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Answer"
        Me.ColumnHeader2.Width = 50
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Choices"
        Me.ColumnHeader1.Width = 400
        '
        'cboID
        '
        Me.cboID.FormattingEnabled = True
        Me.cboID.Location = New System.Drawing.Point(6, 9)
        Me.cboID.Name = "cboID"
        Me.cboID.Size = New System.Drawing.Size(73, 21)
        Me.cboID.TabIndex = 140
        Me.cboID.Visible = False
        '
        'cboLessons
        '
        Me.cboLessons.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLessons.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLessons.FormattingEnabled = True
        Me.cboLessons.Location = New System.Drawing.Point(100, 19)
        Me.cboLessons.Name = "cboLessons"
        Me.cboLessons.Size = New System.Drawing.Size(434, 24)
        Me.cboLessons.TabIndex = 124
        '
        'lblLesson
        '
        Me.lblLesson.AutoSize = True
        Me.lblLesson.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLesson.Location = New System.Drawing.Point(17, 23)
        Me.lblLesson.Name = "lblLesson"
        Me.lblLesson.Size = New System.Drawing.Size(81, 16)
        Me.lblLesson.TabIndex = 139
        Me.lblLesson.Text = "Lesson Title:"
        '
        'lblAnswer1
        '
        Me.lblAnswer1.AutoSize = True
        Me.lblAnswer1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnswer1.Location = New System.Drawing.Point(22, 113)
        Me.lblAnswer1.Name = "lblAnswer1"
        Me.lblAnswer1.Size = New System.Drawing.Size(57, 16)
        Me.lblAnswer1.TabIndex = 135
        Me.lblAnswer1.Text = "Answers"
        '
        'txtAnswer1
        '
        Me.txtAnswer1.Location = New System.Drawing.Point(100, 106)
        Me.txtAnswer1.Multiline = True
        Me.txtAnswer1.Name = "txtAnswer1"
        Me.txtAnswer1.Size = New System.Drawing.Size(434, 33)
        Me.txtAnswer1.TabIndex = 126
        '
        'txtQuestionText
        '
        Me.txtQuestionText.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQuestionText.Location = New System.Drawing.Point(100, 54)
        Me.txtQuestionText.Multiline = True
        Me.txtQuestionText.Name = "txtQuestionText"
        Me.txtQuestionText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtQuestionText.Size = New System.Drawing.Size(434, 46)
        Me.txtQuestionText.TabIndex = 125
        '
        'txtQuestion
        '
        Me.txtQuestion.AutoSize = True
        Me.txtQuestion.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQuestion.Location = New System.Drawing.Point(5, 57)
        Me.txtQuestion.Name = "txtQuestion"
        Me.txtQuestion.Size = New System.Drawing.Size(92, 16)
        Me.txtQuestion.TabIndex = 134
        Me.txtQuestion.Text = "Question Text:"
        '
        'frmNewQ
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(554, 417)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnSaveUpdate)
        Me.Controls.Add(Me.btnCancel)
        Me.Name = "frmNewQ"
        Me.Text = "frmNewQ"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Public WithEvents btnSaveUpdate As System.Windows.Forms.Button
    Private WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Private WithEvents cboLessons As System.Windows.Forms.ComboBox
    Private WithEvents lblLesson As System.Windows.Forms.Label
    Private WithEvents lblAnswer1 As System.Windows.Forms.Label
    Private WithEvents txtAnswer1 As System.Windows.Forms.TextBox
    Private WithEvents txtQuestionText As System.Windows.Forms.TextBox
    Private WithEvents txtQuestion As System.Windows.Forms.Label
    Friend WithEvents cboID As System.Windows.Forms.ComboBox
    Friend WithEvents lvList As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Private WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
End Class
