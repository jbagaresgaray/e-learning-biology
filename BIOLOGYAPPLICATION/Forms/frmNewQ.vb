Option Explicit On

Imports System.Data.OleDb
Imports System.Data
Imports System.IO
Imports System.Windows.Forms

Friend Class frmNewQ
    Public State As FormState

    Dim QID As Integer

    Private Sub btnSaveUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveUpdate.Click
        If ExecuteQuery("INSERT INTO tbl_questions (questiontitle, createdate, userid, lessonid) VALUES ('" & Trim(txtQuestionText.Text) & "' " _
                        & ", '" & DateTime.Now.ToString("yyyy-MM-dd") & "', " & CURRENTUSER.IDUSER & ", '" & CInt(cboID.Text) & "')") Then
        End If

        'QID = 0
        'QID = getlastid("tbl_questions")

        Dim sID As String
        sID = "SELECT LAST_INSERT_ID() AS v FROM tbl_questions"
        Dim con As New OleDbConnection(DB_CONNECTION_STRING)
        con.Open()

        Dim com As New OleDbCommand(sID, con)
        Dim read As OleDbDataReader = com.ExecuteReader

        read.Read()
        If read.HasRows Then
            QID = read(0).ToString()
        End If
        read.Close()

        con.Close()

        InsertUpdateAns()
    End Sub

    Private Sub frmNewQ_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        fillCombo(cboLessons, "SELECT lessontitle FROM tbl_lessons")
    End Sub

    Private Sub cboLessons_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLessons.SelectedIndexChanged
        Dim con As New OleDbConnection(DB_CONNECTION_STRING)
        con.Open()

        Dim sID As String = "SELECT lessonid FROM tbl_lessons WHERE lessontitle='" & cboLessons.Text & "'"
        Dim cmd As New OleDbCommand(sID, con)
        Dim dr As OleDbDataReader = cmd.ExecuteReader

        While dr.Read()
            cboID.Text = dr(0).ToString
        End While

        dr.Close()
    End Sub

    Private Sub InsertUpdateAns()

        For i = 0 To lvList.Items.Count - 1
            If ExecuteQuery("INSERT INTO tbl_answers (answertext, createdate, userid, questionid, isCorrect) VALUES ('" & lvList.Items(i).Text & "', '" & DateTime.Now.ToString("yyyy-MM-dd") & "', " _
                            & "" & CURRENTUSER.IDUSER & ", " & QID & ")") Then
            End If
        Next
    End Sub

    Public Sub ShowForm()
        State = FormState.addState
        Me.ShowDialog()
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim item As New ListViewItem
        item.SubItems.Add(Trim(txtAnswer1.Text))
        lvList.Items.Add(item)

        txtAnswer1.Clear()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click

    End Sub
End Class
