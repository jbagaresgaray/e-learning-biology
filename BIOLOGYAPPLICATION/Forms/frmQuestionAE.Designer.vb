Namespace FORMS
    Partial Class frmQuestionAE
        ''' <summary>
        ''' Required designer variable.
        ''' </summary>
        Private components As System.ComponentModel.IContainer = Nothing

        ''' <summary>
        ''' Clean up any resources being used.
        ''' </summary>
        ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        Protected Overrides Sub Dispose(disposing As Boolean)
            If disposing AndAlso (components IsNot Nothing) Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

#Region "Windows Form Designer generated code"

        ''' <summary>
        ''' Required method for Designer support - do not modify
        ''' the contents of this method with the code editor.
        ''' </summary>
        Private Sub InitializeComponent()
            Me.btnSaveUpdate = New System.Windows.Forms.Button()
            Me.btnCancel = New System.Windows.Forms.Button()
            Me.txtQuestionText = New System.Windows.Forms.TextBox()
            Me.txtQuestion = New System.Windows.Forms.Label()
            Me.txtAnswer1 = New System.Windows.Forms.TextBox()
            Me.lblAnswer1 = New System.Windows.Forms.Label()
            Me.chkAnswer1 = New System.Windows.Forms.CheckBox()
            Me.chkAnswer2 = New System.Windows.Forms.CheckBox()
            Me.label1 = New System.Windows.Forms.Label()
            Me.txtAnswer2 = New System.Windows.Forms.TextBox()
            Me.chkAnswer3 = New System.Windows.Forms.CheckBox()
            Me.label2 = New System.Windows.Forms.Label()
            Me.txtAnswer3 = New System.Windows.Forms.TextBox()
            Me.chkAnswer4 = New System.Windows.Forms.CheckBox()
            Me.label3 = New System.Windows.Forms.Label()
            Me.txtAnswer4 = New System.Windows.Forms.TextBox()
            Me.groupBox1 = New System.Windows.Forms.GroupBox()
            Me.lblLesson = New System.Windows.Forms.Label()
            Me.cboLessons = New System.Windows.Forms.ComboBox()
            Me.SuspendLayout()
            '
            ' btnSaveUpdate
            '
            Me.btnSaveUpdate.Location = New System.Drawing.Point(385, 306)
            Me.btnSaveUpdate.Name = "btnSaveUpdate"
            Me.btnSaveUpdate.Size = New System.Drawing.Size(83, 27)
            Me.btnSaveUpdate.TabIndex = 7
            Me.btnSaveUpdate.Text = "Save"
            Me.btnSaveUpdate.UseVisualStyleBackColor = True
            AddHandler Me.btnSaveUpdate.Click, New System.EventHandler(AddressOf Me.btnSave_Click)
            '
            ' btnCancel
            '
            Me.btnCancel.Location = New System.Drawing.Point(469, 306)
            Me.btnCancel.Name = "btnCancel"
            Me.btnCancel.Size = New System.Drawing.Size(83, 27)
            Me.btnCancel.TabIndex = 8
            Me.btnCancel.Text = "Cancel"
            Me.btnCancel.UseVisualStyleBackColor = True
            AddHandler Me.btnCancel.Click, New System.EventHandler(AddressOf Me.btnCancel_Click)
            '
            ' txtQuestionText
            '
            Me.txtQuestionText.Font = New System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
            Me.txtQuestionText.Location = New System.Drawing.Point(118, 53)
            Me.txtQuestionText.Multiline = True
            Me.txtQuestionText.Name = "txtQuestionText"
            Me.txtQuestionText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.txtQuestionText.Size = New System.Drawing.Size(434, 46)
            Me.txtQuestionText.TabIndex = 2
            '
            ' txtQuestion
            '
            Me.txtQuestion.AutoSize = True
            Me.txtQuestion.Font = New System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
            Me.txtQuestion.Location = New System.Drawing.Point(23, 56)
            Me.txtQuestion.Name = "txtQuestion"
            Me.txtQuestion.Size = New System.Drawing.Size(92, 16)
            Me.txtQuestion.TabIndex = 90
            Me.txtQuestion.Text = "Question Text:"
            '
            ' txtAnswer1
            '
            Me.txtAnswer1.Location = New System.Drawing.Point(118, 105)
            Me.txtAnswer1.Multiline = True
            Me.txtAnswer1.Name = "txtAnswer1"
            Me.txtAnswer1.Size = New System.Drawing.Size(267, 33)
            Me.txtAnswer1.TabIndex = 3
            '
            ' lblAnswer1
            '
            Me.lblAnswer1.AutoSize = True
            Me.lblAnswer1.Font = New System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
            Me.lblAnswer1.Location = New System.Drawing.Point(40, 112)
            Me.lblAnswer1.Name = "lblAnswer1"
            Me.lblAnswer1.Size = New System.Drawing.Size(76, 16)
            Me.lblAnswer1.TabIndex = 92
            Me.lblAnswer1.Text = "Answer #1:"
            '
            ' chkAnswer1
            '
            Me.chkAnswer1.AutoSize = True
            Me.chkAnswer1.Location = New System.Drawing.Point(397, 115)
            Me.chkAnswer1.Name = "chkAnswer1"
            Me.chkAnswer1.Size = New System.Drawing.Size(101, 17)
            Me.chkAnswer1.TabIndex = 3
            Me.chkAnswer1.TabStop = False
            Me.chkAnswer1.Text = "Correct Answer"
            Me.chkAnswer1.UseVisualStyleBackColor = True
            '
            ' chkAnswer2
            '
            Me.chkAnswer2.AutoSize = True
            Me.chkAnswer2.Location = New System.Drawing.Point(397, 154)
            Me.chkAnswer2.Name = "chkAnswer2"
            Me.chkAnswer2.Size = New System.Drawing.Size(101, 17)
            Me.chkAnswer2.TabIndex = 5
            Me.chkAnswer2.TabStop = False
            Me.chkAnswer2.Text = "Correct Answer"
            Me.chkAnswer2.UseVisualStyleBackColor = True
            '
            ' label1
            '
            Me.label1.AutoSize = True
            Me.label1.Font = New System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
            Me.label1.Location = New System.Drawing.Point(40, 151)
            Me.label1.Name = "label1"
            Me.label1.Size = New System.Drawing.Size(76, 16)
            Me.label1.TabIndex = 95
            Me.label1.Text = "Answer #2:"
            '
            ' txtAnswer2
            '
            Me.txtAnswer2.Location = New System.Drawing.Point(118, 144)
            Me.txtAnswer2.Multiline = True
            Me.txtAnswer2.Name = "txtAnswer2"
            Me.txtAnswer2.Size = New System.Drawing.Size(267, 33)
            Me.txtAnswer2.TabIndex = 4
            '
            ' chkAnswer3
            '
            Me.chkAnswer3.AutoSize = True
            Me.chkAnswer3.Location = New System.Drawing.Point(397, 193)
            Me.chkAnswer3.Name = "chkAnswer3"
            Me.chkAnswer3.Size = New System.Drawing.Size(101, 17)
            Me.chkAnswer3.TabIndex = 7
            Me.chkAnswer3.TabStop = False
            Me.chkAnswer3.Text = "Correct Answer"
            Me.chkAnswer3.UseVisualStyleBackColor = True
            '
            ' label2
            '
            Me.label2.AutoSize = True
            Me.label2.Font = New System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
            Me.label2.Location = New System.Drawing.Point(40, 190)
            Me.label2.Name = "label2"
            Me.label2.Size = New System.Drawing.Size(76, 16)
            Me.label2.TabIndex = 98
            Me.label2.Text = "Answer #3:"
            '
            ' txtAnswer3
            '
            Me.txtAnswer3.Location = New System.Drawing.Point(118, 183)
            Me.txtAnswer3.Multiline = True
            Me.txtAnswer3.Name = "txtAnswer3"
            Me.txtAnswer3.Size = New System.Drawing.Size(267, 33)
            Me.txtAnswer3.TabIndex = 5
            '
            ' chkAnswer4
            '
            Me.chkAnswer4.AutoSize = True
            Me.chkAnswer4.Location = New System.Drawing.Point(397, 232)
            Me.chkAnswer4.Name = "chkAnswer4"
            Me.chkAnswer4.Size = New System.Drawing.Size(101, 17)
            Me.chkAnswer4.TabIndex = 9
            Me.chkAnswer4.TabStop = False
            Me.chkAnswer4.Text = "Correct Answer"
            Me.chkAnswer4.UseVisualStyleBackColor = True
            '
            ' label3
            '
            Me.label3.AutoSize = True
            Me.label3.Font = New System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
            Me.label3.Location = New System.Drawing.Point(40, 229)
            Me.label3.Name = "label3"
            Me.label3.Size = New System.Drawing.Size(76, 16)
            Me.label3.TabIndex = 101
            Me.label3.Text = "Answer #4:"
            '
            ' txtAnswer4
            '
            Me.txtAnswer4.Location = New System.Drawing.Point(118, 222)
            Me.txtAnswer4.Multiline = True
            Me.txtAnswer4.Name = "txtAnswer4"
            Me.txtAnswer4.Size = New System.Drawing.Size(267, 33)
            Me.txtAnswer4.TabIndex = 6
            '
            ' groupBox1
            '
            Me.groupBox1.Location = New System.Drawing.Point(-13, 289)
            Me.groupBox1.Name = "groupBox1"
            Me.groupBox1.Size = New System.Drawing.Size(755, 5)
            Me.groupBox1.TabIndex = 103
            Me.groupBox1.TabStop = False
            '
            ' lblLesson
            '
            Me.lblLesson.AutoSize = True
            Me.lblLesson.Font = New System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
            Me.lblLesson.Location = New System.Drawing.Point(35, 22)
            Me.lblLesson.Name = "lblLesson"
            Me.lblLesson.Size = New System.Drawing.Size(81, 16)
            Me.lblLesson.TabIndex = 104
            Me.lblLesson.Text = "Lesson Title:"
            '
            ' cboLessons
            '
            Me.cboLessons.Font = New System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
            Me.cboLessons.FormattingEnabled = True
            Me.cboLessons.Location = New System.Drawing.Point(118, 18)
            Me.cboLessons.Name = "cboLessons"
            Me.cboLessons.Size = New System.Drawing.Size(434, 24)
            Me.cboLessons.TabIndex = 1
            AddHandler Me.cboLessons.DropDown, New System.EventHandler(AddressOf Me.cboLessons_DropDown)
            AddHandler Me.cboLessons.SelectedIndexChanged, New System.EventHandler(AddressOf Me.cboLessons_SelectedIndexChanged)
            AddHandler Me.cboLessons.KeyDown, New System.Windows.Forms.KeyEventHandler(AddressOf Me.cboLessons_KeyDown)
            '
            ' AddEditQuestion
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0F, 13.0F)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(579, 349)
            Me.Controls.Add(Me.cboLessons)
            Me.Controls.Add(Me.lblLesson)
            Me.Controls.Add(Me.groupBox1)
            Me.Controls.Add(Me.chkAnswer4)
            Me.Controls.Add(Me.label3)
            Me.Controls.Add(Me.txtAnswer4)
            Me.Controls.Add(Me.chkAnswer3)
            Me.Controls.Add(Me.label2)
            Me.Controls.Add(Me.txtAnswer3)
            Me.Controls.Add(Me.chkAnswer2)
            Me.Controls.Add(Me.label1)
            Me.Controls.Add(Me.txtAnswer2)
            Me.Controls.Add(Me.chkAnswer1)
            Me.Controls.Add(Me.lblAnswer1)
            Me.Controls.Add(Me.txtAnswer1)
            Me.Controls.Add(Me.txtQuestionText)
            Me.Controls.Add(Me.txtQuestion)
            Me.Controls.Add(Me.btnSaveUpdate)
            Me.Controls.Add(Me.btnCancel)
            Me.Font = New System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
            Me.KeyPreview = True
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "AddEditQuestion"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
            AddHandler Me.Load, New System.EventHandler(AddressOf Me.AEQuestion_Load)
            AddHandler Me.KeyPress, New System.Windows.Forms.KeyPressEventHandler(AddressOf Me.AddEditQuestion_KeyPress)
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        Public btnSaveUpdate As System.Windows.Forms.Button
        Private btnCancel As System.Windows.Forms.Button
        Private txtQuestionText As System.Windows.Forms.TextBox
        Private txtQuestion As System.Windows.Forms.Label
        Private txtAnswer1 As System.Windows.Forms.TextBox
        Private lblAnswer1 As System.Windows.Forms.Label
        Private chkAnswer1 As System.Windows.Forms.CheckBox
        Private chkAnswer2 As System.Windows.Forms.CheckBox
        Private label1 As System.Windows.Forms.Label
        Private txtAnswer2 As System.Windows.Forms.TextBox
        Private chkAnswer3 As System.Windows.Forms.CheckBox
        Private label2 As System.Windows.Forms.Label
        Private txtAnswer3 As System.Windows.Forms.TextBox
        Private chkAnswer4 As System.Windows.Forms.CheckBox
        Private label3 As System.Windows.Forms.Label
        Private txtAnswer4 As System.Windows.Forms.TextBox
        Private groupBox1 As System.Windows.Forms.GroupBox
        Private lblLesson As System.Windows.Forms.Label
        Private cboLessons As System.Windows.Forms.ComboBox
    End Class
End Namespace
