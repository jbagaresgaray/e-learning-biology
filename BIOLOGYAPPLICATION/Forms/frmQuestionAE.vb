
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Windows.Forms

Namespace FORMS
    Partial Public Class frmQuestionAE
        Inherits Form

        Public Event RefreshListView As EventHandler
        Private m_recordId As String
        Private m_answerObj As List(Of Answer)

        Public Property recordId() As String
            Get
                Return m_recordId
            End Get
            Set(value As String)
                m_recordId = value
            End Set
        End Property

        'Public Property answerObj() As List(Of Answer)
        '    Get
        '        Return m_answerObj
        '    End Get
        '    Set(value As List(Of Answer))
        '        m_answerObj = value
        '    End Set
        'End Property


        Public Sub New()
            InitializeComponent()
        End Sub

        Private Sub btnCancel_Click(sender As Object, e As EventArgs)
            Me.Close()
        End Sub

        Private Sub AddEditQuestion_KeyPress(sender As Object, e As KeyPressEventArgs)

        End Sub

        Private Sub btnSave_Click(sender As Object, e As EventArgs)
            If cboLessons.Text <> "" Then
                Dim q As New QuestionsInfo()
                Dim list As New List(Of Answer)()
                Dim answer1 As New Answer()
                answer1.AnswerText = txtAnswer1.Text
                answer1.isCorrect = chkAnswer1.Checked
                Dim answer2 As New Answer()
                answer2.AnswerText = txtAnswer2.Text
                answer2.isCorrect = chkAnswer2.Checked
                Dim answer3 As New Answer()
                answer3.AnswerText = txtAnswer3.Text
                answer3.isCorrect = chkAnswer3.Checked
                Dim answer4 As New Answer()
                answer4.AnswerText = txtAnswer4.Text
                answer4.isCorrect = chkAnswer4.Checked
                q.lessonId = Convert.ToInt32(GetString("SELECT lessonid FROM qry_lessons WHERE lessontitle LIKE '" & cboLessons.Text & "'"))
                q.questionTitle = txtQuestionText.Text.Replace("'", " ")

                If btnSaveUpdate.Text = "Update" Then
                    'answer1.answerId = answerObj(0).answerId
                    'answer2.answerId = answerObj(1).answerId
                    'answer3.answerId = answerObj(2).answerId
                    'answer4.answerId = answerObj(3).answerId
                    list.Add(answer1)
                    list.Add(answer2)
                    list.Add(answer3)
                    list.Add(answer4)
                    q.questionId = Convert.ToInt32(Me.recordId)
                    If UpdateQuestion(q, list) > 0 Then
                        PromptsMessage(2)
                        Me.Close()
                        RaiseEvent RefreshListView(sender, e)
                    End If
                Else
                    list.Add(answer1)
                    list.Add(answer2)
                    list.Add(answer3)
                    list.Add(answer4)

                    If CreateNewQuestion(q, list) > 0 Then
                        PromptsMessage(1)
                        Me.Close()
                        RaiseEvent RefreshListView(sender, e)
                    End If
                End If
            Else
                PromptsMessage(9, "Lesson Field")
                Return
            End If
        End Sub

        Private Sub AEQuestion_Load(sender As Object, e As EventArgs)
            cboLessons.Items.Clear()
            For Each lesson As LessonInfo In GetLessons()
                cboLessons.Items.Add(lesson.lessontitle.ToString())
            Next

            If btnSaveUpdate.Text = "Update" Then
                Dim q As New QuestionsInfo()
                q = GetQuestion(Me.recordId.ToString())
                txtQuestionText.Text = q.questionTitle
                cboLessons.Text = GetString("SELECT lessontitle FROM tbl_lessons WHERE lessonid =" & q.lessonid)
                Dim ListOfAnswers As List(Of Answer) = GetAnswers(recordId.ToString())
                txtAnswer1.Text = ListOfAnswers(0).AnswerText.Replace("'", " ")
                txtAnswer2.Text = ListOfAnswers(1).AnswerText.Replace("'", " ")
                txtAnswer3.Text = ListOfAnswers(2).AnswerText.Replace("'", " ")
                txtAnswer4.Text = ListOfAnswers(3).AnswerText.Replace("'", " ")
                chkAnswer1.Checked = ListOfAnswers(0).isCorrect
                chkAnswer2.Checked = ListOfAnswers(1).isCorrect
                chkAnswer3.Checked = ListOfAnswers(2).isCorrect
                chkAnswer4.Checked = ListOfAnswers(3).isCorrect

                'Me.answerObj = ListOfAnswers
            End If
        End Sub

        Private Sub cboLessons_DropDown(sender As Object, e As EventArgs)

        End Sub

        Private Sub cboLessons_SelectedIndexChanged(sender As Object, e As EventArgs)

        End Sub

        Private Sub cboLessons_KeyDown(sender As Object, e As KeyEventArgs)
            e.SuppressKeyPress = True
        End Sub
    End Class
End Namespace
