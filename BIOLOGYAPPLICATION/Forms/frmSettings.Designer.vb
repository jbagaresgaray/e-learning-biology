<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSettings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSettings))
        Me.FormSkin1 = New BIOLOGYAPPLICATION.FormSkin()
        Me.FlatTabControl1 = New BIOLOGYAPPLICATION.FlatTabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.lvLessons = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.toolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.cmdAddNewLesson = New System.Windows.Forms.ToolStripButton()
        Me.cmdEditLessons = New System.Windows.Forms.ToolStripButton()
        Me.cmdDeleteLessons = New System.Windows.Forms.ToolStripButton()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.lvQuestions = New System.Windows.Forms.ListView()
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ToolStrip2 = New System.Windows.Forms.ToolStrip()
        Me.cmdAddNewQuestion = New System.Windows.Forms.ToolStripButton()
        Me.cmdEditQuestion = New System.Windows.Forms.ToolStripButton()
        Me.cmdDeleteQuestions = New System.Windows.Forms.ToolStripButton()
        Me.TabPage9 = New System.Windows.Forms.TabPage()
        Me.btnWordDelete = New System.Windows.Forms.Button()
        Me.btnWordRefresh = New System.Windows.Forms.Button()
        Me.btnWordAdd = New System.Windows.Forms.Button()
        Me.lvGames = New System.Windows.Forms.ListView()
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader11 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader12 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader13 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.cbCorrectAnswer = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtOption4 = New System.Windows.Forms.TextBox()
        Me.txtOption3 = New System.Windows.Forms.TextBox()
        Me.txtOption2 = New System.Windows.Forms.TextBox()
        Me.txtOption1 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.lvResults = New System.Windows.Forms.ListView()
        Me.TSTestResults = New System.Windows.Forms.ToolStrip()
        Me.toolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.toolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.toolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.toolStripButton4 = New System.Windows.Forms.ToolStripButton()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.cmdCancel = New BIOLOGYAPPLICATION.FlatButton()
        Me.cmdSave = New BIOLOGYAPPLICATION.FlatButton()
        Me.txtAbout = New System.Windows.Forms.RichTextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TabPage10 = New System.Windows.Forms.TabPage()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.cmdClose = New System.Windows.Forms.Button()
        Me.FormSkin1.SuspendLayout()
        Me.FlatTabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.toolStrip1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.ToolStrip2.SuspendLayout()
        Me.TabPage9.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.TSTestResults.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'FormSkin1
        '
        Me.FormSkin1.BackColor = System.Drawing.Color.White
        Me.FormSkin1.BaseColor = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.FormSkin1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(53, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.FormSkin1.Controls.Add(Me.FlatTabControl1)
        Me.FormSkin1.Controls.Add(Me.Panel1)
        Me.FormSkin1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FormSkin1.FlatColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FormSkin1.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.FormSkin1.HeaderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FormSkin1.HeaderMaximize = False
        Me.FormSkin1.Location = New System.Drawing.Point(0, 0)
        Me.FormSkin1.Name = "FormSkin1"
        Me.FormSkin1.Size = New System.Drawing.Size(923, 585)
        Me.FormSkin1.TabIndex = 0
        Me.FormSkin1.Text = "Settings"
        '
        'FlatTabControl1
        '
        Me.FlatTabControl1.ActiveColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatTabControl1.BaseColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatTabControl1.Controls.Add(Me.TabPage1)
        Me.FlatTabControl1.Controls.Add(Me.TabPage2)
        Me.FlatTabControl1.Controls.Add(Me.TabPage9)
        Me.FlatTabControl1.Controls.Add(Me.TabPage4)
        Me.FlatTabControl1.Controls.Add(Me.TabPage5)
        Me.FlatTabControl1.Controls.Add(Me.TabPage10)
        Me.FlatTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlatTabControl1.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatTabControl1.ItemSize = New System.Drawing.Size(120, 40)
        Me.FlatTabControl1.Location = New System.Drawing.Point(0, 50)
        Me.FlatTabControl1.Name = "FlatTabControl1"
        Me.FlatTabControl1.SelectedIndex = 0
        Me.FlatTabControl1.Size = New System.Drawing.Size(923, 535)
        Me.FlatTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.FlatTabControl1.TabIndex = 2
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.TabPage1.Controls.Add(Me.lvLessons)
        Me.TabPage1.Controls.Add(Me.toolStrip1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 44)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(915, 487)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "LESSONS"
        '
        'lvLessons
        '
        Me.lvLessons.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4})
        Me.lvLessons.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvLessons.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvLessons.FullRowSelect = True
        Me.lvLessons.GridLines = True
        Me.lvLessons.Location = New System.Drawing.Point(3, 29)
        Me.lvLessons.Name = "lvLessons"
        Me.lvLessons.Size = New System.Drawing.Size(909, 455)
        Me.lvLessons.TabIndex = 80
        Me.lvLessons.UseCompatibleStateImageBehavior = False
        Me.lvLessons.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "ID"
        Me.ColumnHeader1.Width = 0
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Title"
        Me.ColumnHeader2.Width = 300
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Description"
        Me.ColumnHeader3.Width = 350
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Instructions"
        Me.ColumnHeader4.Width = 400
        '
        'toolStrip1
        '
        Me.toolStrip1.BackColor = System.Drawing.Color.Transparent
        Me.toolStrip1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.toolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmdAddNewLesson, Me.cmdEditLessons, Me.cmdDeleteLessons})
        Me.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.toolStrip1.Location = New System.Drawing.Point(3, 3)
        Me.toolStrip1.Name = "toolStrip1"
        Me.toolStrip1.Size = New System.Drawing.Size(909, 26)
        Me.toolStrip1.TabIndex = 79
        '
        'cmdAddNewLesson
        '
        Me.cmdAddNewLesson.ForeColor = System.Drawing.Color.White
        Me.cmdAddNewLesson.Image = CType(resources.GetObject("cmdAddNewLesson.Image"), System.Drawing.Image)
        Me.cmdAddNewLesson.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdAddNewLesson.Name = "cmdAddNewLesson"
        Me.cmdAddNewLesson.Size = New System.Drawing.Size(58, 23)
        Me.cmdAddNewLesson.Text = "Add"
        '
        'cmdEditLessons
        '
        Me.cmdEditLessons.ForeColor = System.Drawing.Color.White
        Me.cmdEditLessons.Image = CType(resources.GetObject("cmdEditLessons.Image"), System.Drawing.Image)
        Me.cmdEditLessons.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdEditLessons.Name = "cmdEditLessons"
        Me.cmdEditLessons.Size = New System.Drawing.Size(56, 23)
        Me.cmdEditLessons.Text = "Edit"
        '
        'cmdDeleteLessons
        '
        Me.cmdDeleteLessons.ForeColor = System.Drawing.Color.White
        Me.cmdDeleteLessons.Image = CType(resources.GetObject("cmdDeleteLessons.Image"), System.Drawing.Image)
        Me.cmdDeleteLessons.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdDeleteLessons.Name = "cmdDeleteLessons"
        Me.cmdDeleteLessons.Size = New System.Drawing.Size(73, 23)
        Me.cmdDeleteLessons.Text = "Delete"
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.TabPage2.Controls.Add(Me.lvQuestions)
        Me.TabPage2.Controls.Add(Me.ToolStrip2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 44)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(915, 487)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "QUESTIONS"
        '
        'lvQuestions
        '
        Me.lvQuestions.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader5, Me.ColumnHeader6, Me.ColumnHeader7})
        Me.lvQuestions.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvQuestions.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvQuestions.FullRowSelect = True
        Me.lvQuestions.GridLines = True
        Me.lvQuestions.Location = New System.Drawing.Point(3, 29)
        Me.lvQuestions.Name = "lvQuestions"
        Me.lvQuestions.Size = New System.Drawing.Size(909, 455)
        Me.lvQuestions.TabIndex = 80
        Me.lvQuestions.UseCompatibleStateImageBehavior = False
        Me.lvQuestions.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "ID"
        Me.ColumnHeader5.Width = 0
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Questions"
        Me.ColumnHeader6.Width = 500
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Lesson"
        Me.ColumnHeader7.Width = 400
        '
        'ToolStrip2
        '
        Me.ToolStrip2.BackColor = System.Drawing.Color.Transparent
        Me.ToolStrip2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmdAddNewQuestion, Me.cmdEditQuestion, Me.cmdDeleteQuestions})
        Me.ToolStrip2.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStrip2.Location = New System.Drawing.Point(3, 3)
        Me.ToolStrip2.Name = "ToolStrip2"
        Me.ToolStrip2.Size = New System.Drawing.Size(909, 26)
        Me.ToolStrip2.TabIndex = 78
        '
        'cmdAddNewQuestion
        '
        Me.cmdAddNewQuestion.ForeColor = System.Drawing.Color.White
        Me.cmdAddNewQuestion.Image = CType(resources.GetObject("cmdAddNewQuestion.Image"), System.Drawing.Image)
        Me.cmdAddNewQuestion.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdAddNewQuestion.Name = "cmdAddNewQuestion"
        Me.cmdAddNewQuestion.Size = New System.Drawing.Size(58, 23)
        Me.cmdAddNewQuestion.Text = "Add"
        '
        'cmdEditQuestion
        '
        Me.cmdEditQuestion.ForeColor = System.Drawing.Color.White
        Me.cmdEditQuestion.Image = CType(resources.GetObject("cmdEditQuestion.Image"), System.Drawing.Image)
        Me.cmdEditQuestion.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdEditQuestion.Name = "cmdEditQuestion"
        Me.cmdEditQuestion.Size = New System.Drawing.Size(56, 23)
        Me.cmdEditQuestion.Text = "Edit"
        '
        'cmdDeleteQuestions
        '
        Me.cmdDeleteQuestions.ForeColor = System.Drawing.Color.White
        Me.cmdDeleteQuestions.Image = CType(resources.GetObject("cmdDeleteQuestions.Image"), System.Drawing.Image)
        Me.cmdDeleteQuestions.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdDeleteQuestions.Name = "cmdDeleteQuestions"
        Me.cmdDeleteQuestions.Size = New System.Drawing.Size(73, 23)
        Me.cmdDeleteQuestions.Text = "Delete"
        '
        'TabPage9
        '
        Me.TabPage9.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.TabPage9.Controls.Add(Me.btnWordDelete)
        Me.TabPage9.Controls.Add(Me.btnWordRefresh)
        Me.TabPage9.Controls.Add(Me.btnWordAdd)
        Me.TabPage9.Controls.Add(Me.lvGames)
        Me.TabPage9.Controls.Add(Me.TextBox1)
        Me.TabPage9.Controls.Add(Me.cbCorrectAnswer)
        Me.TabPage9.Controls.Add(Me.Label6)
        Me.TabPage9.Controls.Add(Me.GroupBox1)
        Me.TabPage9.Controls.Add(Me.Label8)
        Me.TabPage9.Location = New System.Drawing.Point(4, 44)
        Me.TabPage9.Name = "TabPage9"
        Me.TabPage9.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage9.Size = New System.Drawing.Size(915, 487)
        Me.TabPage9.TabIndex = 2
        Me.TabPage9.Text = "GAMES"
        '
        'btnWordDelete
        '
        Me.btnWordDelete.Location = New System.Drawing.Point(811, 28)
        Me.btnWordDelete.Name = "btnWordDelete"
        Me.btnWordDelete.Size = New System.Drawing.Size(83, 27)
        Me.btnWordDelete.TabIndex = 119
        Me.btnWordDelete.Text = "Delete"
        Me.btnWordDelete.UseVisualStyleBackColor = True
        '
        'btnWordRefresh
        '
        Me.btnWordRefresh.Location = New System.Drawing.Point(728, 28)
        Me.btnWordRefresh.Name = "btnWordRefresh"
        Me.btnWordRefresh.Size = New System.Drawing.Size(83, 27)
        Me.btnWordRefresh.TabIndex = 120
        Me.btnWordRefresh.Text = "Refresh"
        Me.btnWordRefresh.UseVisualStyleBackColor = True
        '
        'btnWordAdd
        '
        Me.btnWordAdd.Location = New System.Drawing.Point(633, 28)
        Me.btnWordAdd.Name = "btnWordAdd"
        Me.btnWordAdd.Size = New System.Drawing.Size(83, 27)
        Me.btnWordAdd.TabIndex = 118
        Me.btnWordAdd.Text = "Add"
        Me.btnWordAdd.UseVisualStyleBackColor = True
        '
        'lvGames
        '
        Me.lvGames.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader8, Me.ColumnHeader9, Me.ColumnHeader10, Me.ColumnHeader11, Me.ColumnHeader12, Me.ColumnHeader13})
        Me.lvGames.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lvGames.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvGames.FullRowSelect = True
        Me.lvGames.GridLines = True
        Me.lvGames.Location = New System.Drawing.Point(3, 165)
        Me.lvGames.Name = "lvGames"
        Me.lvGames.Size = New System.Drawing.Size(909, 319)
        Me.lvGames.Sorting = System.Windows.Forms.SortOrder.Descending
        Me.lvGames.TabIndex = 117
        Me.lvGames.TabStop = False
        Me.lvGames.UseCompatibleStateImageBehavior = False
        Me.lvGames.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Question"
        Me.ColumnHeader8.Width = 171
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "A"
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "B"
        '
        'ColumnHeader11
        '
        Me.ColumnHeader11.Text = "C"
        '
        'ColumnHeader12
        '
        Me.ColumnHeader12.Text = "D"
        '
        'ColumnHeader13
        '
        Me.ColumnHeader13.Text = "Answer"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(18, 30)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox1.Size = New System.Drawing.Size(380, 58)
        Me.TextBox1.TabIndex = 14
        '
        'cbCorrectAnswer
        '
        Me.cbCorrectAnswer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbCorrectAnswer.FormattingEnabled = True
        Me.cbCorrectAnswer.Location = New System.Drawing.Point(414, 30)
        Me.cbCorrectAnswer.Name = "cbCorrectAnswer"
        Me.cbCorrectAnswer.Size = New System.Drawing.Size(178, 25)
        Me.cbCorrectAnswer.TabIndex = 17
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(410, 3)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(106, 19)
        Me.Label6.TabIndex = 16
        Me.Label6.Text = "Correct Answer:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtOption4)
        Me.GroupBox1.Controls.Add(Me.txtOption3)
        Me.GroupBox1.Controls.Add(Me.txtOption2)
        Me.GroupBox1.Controls.Add(Me.txtOption1)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(15, 94)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(890, 65)
        Me.GroupBox1.TabIndex = 15
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Options:"
        '
        'txtOption4
        '
        Me.txtOption4.Location = New System.Drawing.Point(686, 26)
        Me.txtOption4.Name = "txtOption4"
        Me.txtOption4.Size = New System.Drawing.Size(176, 25)
        Me.txtOption4.TabIndex = 10
        '
        'txtOption3
        '
        Me.txtOption3.Location = New System.Drawing.Point(469, 26)
        Me.txtOption3.Name = "txtOption3"
        Me.txtOption3.Size = New System.Drawing.Size(176, 25)
        Me.txtOption3.TabIndex = 8
        '
        'txtOption2
        '
        Me.txtOption2.Location = New System.Drawing.Point(256, 26)
        Me.txtOption2.Name = "txtOption2"
        Me.txtOption2.Size = New System.Drawing.Size(176, 25)
        Me.txtOption2.TabIndex = 6
        '
        'txtOption1
        '
        Me.txtOption1.Location = New System.Drawing.Point(41, 28)
        Me.txtOption1.Name = "txtOption1"
        Me.txtOption1.Size = New System.Drawing.Size(176, 25)
        Me.txtOption1.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(662, 29)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(17, 19)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "4"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(445, 29)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(17, 19)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "3"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(232, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(17, 19)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "2"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(17, 29)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(17, 19)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "1"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(14, 9)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(68, 19)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Question:"
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.TabPage4.Controls.Add(Me.lvResults)
        Me.TabPage4.Controls.Add(Me.TSTestResults)
        Me.TabPage4.Location = New System.Drawing.Point(4, 44)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(915, 487)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "RESULTS"
        '
        'lvResults
        '
        Me.lvResults.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvResults.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvResults.FullRowSelect = True
        Me.lvResults.GridLines = True
        Me.lvResults.Location = New System.Drawing.Point(3, 34)
        Me.lvResults.Name = "lvResults"
        Me.lvResults.Size = New System.Drawing.Size(909, 450)
        Me.lvResults.TabIndex = 77
        Me.lvResults.UseCompatibleStateImageBehavior = False
        '
        'TSTestResults
        '
        Me.TSTestResults.BackColor = System.Drawing.Color.Transparent
        Me.TSTestResults.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TSTestResults.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.TSTestResults.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolStripButton3, Me.toolStripButton1, Me.toolStripButton2, Me.toolStripButton4})
        Me.TSTestResults.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.TSTestResults.Location = New System.Drawing.Point(3, 3)
        Me.TSTestResults.Name = "TSTestResults"
        Me.TSTestResults.Size = New System.Drawing.Size(909, 31)
        Me.TSTestResults.TabIndex = 76
        '
        'toolStripButton3
        '
        Me.toolStripButton3.ForeColor = System.Drawing.Color.White
        Me.toolStripButton3.Image = CType(resources.GetObject("toolStripButton3.Image"), System.Drawing.Image)
        Me.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolStripButton3.Name = "toolStripButton3"
        Me.toolStripButton3.Size = New System.Drawing.Size(73, 28)
        Me.toolStripButton3.Text = "Delete"
        '
        'toolStripButton1
        '
        Me.toolStripButton1.ForeColor = System.Drawing.Color.White
        Me.toolStripButton1.Image = CType(resources.GetObject("toolStripButton1.Image"), System.Drawing.Image)
        Me.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolStripButton1.Name = "toolStripButton1"
        Me.toolStripButton1.Size = New System.Drawing.Size(58, 28)
        Me.toolStripButton1.Text = "Add"
        Me.toolStripButton1.Visible = False
        '
        'toolStripButton2
        '
        Me.toolStripButton2.ForeColor = System.Drawing.Color.White
        Me.toolStripButton2.Image = CType(resources.GetObject("toolStripButton2.Image"), System.Drawing.Image)
        Me.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolStripButton2.Name = "toolStripButton2"
        Me.toolStripButton2.Size = New System.Drawing.Size(56, 28)
        Me.toolStripButton2.Text = "Edit"
        Me.toolStripButton2.Visible = False
        '
        'toolStripButton4
        '
        Me.toolStripButton4.ForeColor = System.Drawing.Color.White
        Me.toolStripButton4.Image = CType(resources.GetObject("toolStripButton4.Image"), System.Drawing.Image)
        Me.toolStripButton4.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolStripButton4.Name = "toolStripButton4"
        Me.toolStripButton4.Size = New System.Drawing.Size(90, 28)
        Me.toolStripButton4.Text = "Refresh"
        '
        'TabPage5
        '
        Me.TabPage5.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.TabPage5.Controls.Add(Me.cmdCancel)
        Me.TabPage5.Controls.Add(Me.cmdSave)
        Me.TabPage5.Controls.Add(Me.txtAbout)
        Me.TabPage5.Controls.Add(Me.Label2)
        Me.TabPage5.Location = New System.Drawing.Point(4, 44)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(915, 487)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "ABOUT"
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.Color.Transparent
        Me.cmdCancel.BaseColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(76, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdCancel.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.cmdCancel.Location = New System.Drawing.Point(785, 447)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Rounded = True
        Me.cmdCancel.Size = New System.Drawing.Size(106, 32)
        Me.cmdCancel.TabIndex = 115
        Me.cmdCancel.Text = "Cancel"
        Me.cmdCancel.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'cmdSave
        '
        Me.cmdSave.BackColor = System.Drawing.Color.Transparent
        Me.cmdSave.BaseColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.cmdSave.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSave.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.cmdSave.Location = New System.Drawing.Point(673, 447)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Rounded = True
        Me.cmdSave.Size = New System.Drawing.Size(106, 32)
        Me.cmdSave.TabIndex = 114
        Me.cmdSave.Text = "Save"
        Me.cmdSave.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'txtAbout
        '
        Me.txtAbout.Location = New System.Drawing.Point(8, 35)
        Me.txtAbout.Name = "txtAbout"
        Me.txtAbout.Size = New System.Drawing.Size(899, 406)
        Me.txtAbout.TabIndex = 113
        Me.txtAbout.Text = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(7, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 16)
        Me.Label2.TabIndex = 110
        Me.Label2.Text = "About Biology:"
        '
        'TabPage10
        '
        Me.TabPage10.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.TabPage10.Location = New System.Drawing.Point(4, 44)
        Me.TabPage10.Name = "TabPage10"
        Me.TabPage10.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage10.Size = New System.Drawing.Size(915, 487)
        Me.TabPage10.TabIndex = 5
        Me.TabPage10.Text = "USER MANAGEMENT"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(923, 50)
        Me.Panel1.TabIndex = 1
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.cmdClose)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel3.Location = New System.Drawing.Point(873, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(50, 50)
        Me.Panel3.TabIndex = 3
        '
        'cmdClose
        '
        Me.cmdClose.BackgroundImage = CType(resources.GetObject("cmdClose.BackgroundImage"), System.Drawing.Image)
        Me.cmdClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.cmdClose.FlatAppearance.BorderSize = 0
        Me.cmdClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdClose.Location = New System.Drawing.Point(3, 3)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.Size = New System.Drawing.Size(44, 45)
        Me.cmdClose.TabIndex = 0
        Me.cmdClose.UseVisualStyleBackColor = True
        '
        'frmSettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(923, 585)
        Me.Controls.Add(Me.FormSkin1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSettings"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Settings"
        Me.TransparencyKey = System.Drawing.Color.Fuchsia
        Me.FormSkin1.ResumeLayout(False)
        Me.FlatTabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.toolStrip1.ResumeLayout(False)
        Me.toolStrip1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.ToolStrip2.ResumeLayout(False)
        Me.ToolStrip2.PerformLayout()
        Me.TabPage9.ResumeLayout(False)
        Me.TabPage9.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.TSTestResults.ResumeLayout(False)
        Me.TSTestResults.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents FormSkin1 As BIOLOGYAPPLICATION.FormSkin
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents FlatTabControl1 As BIOLOGYAPPLICATION.FlatTabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents lvQuestions As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Private WithEvents ToolStrip2 As System.Windows.Forms.ToolStrip
    Private WithEvents cmdAddNewQuestion As System.Windows.Forms.ToolStripButton
    Private WithEvents cmdEditQuestion As System.Windows.Forms.ToolStripButton
    Private WithEvents cmdDeleteQuestions As System.Windows.Forms.ToolStripButton
    Friend WithEvents TabPage9 As System.Windows.Forms.TabPage
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents cbCorrectAnswer As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtOption4 As System.Windows.Forms.TextBox
    Friend WithEvents txtOption3 As System.Windows.Forms.TextBox
    Friend WithEvents txtOption2 As System.Windows.Forms.TextBox
    Friend WithEvents txtOption1 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Private WithEvents lvGames As System.Windows.Forms.ListView
    Public WithEvents btnWordDelete As System.Windows.Forms.Button
    Private WithEvents btnWordRefresh As System.Windows.Forms.Button
    Public WithEvents btnWordAdd As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents cmdClose As System.Windows.Forms.Button
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage10 As System.Windows.Forms.TabPage
    Friend WithEvents lvLessons As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Private WithEvents toolStrip1 As System.Windows.Forms.ToolStrip
    Private WithEvents cmdAddNewLesson As System.Windows.Forms.ToolStripButton
    Private WithEvents cmdEditLessons As System.Windows.Forms.ToolStripButton
    Private WithEvents cmdDeleteLessons As System.Windows.Forms.ToolStripButton
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader9 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader10 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader11 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader12 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader13 As System.Windows.Forms.ColumnHeader
    Private WithEvents TSTestResults As System.Windows.Forms.ToolStrip
    Private WithEvents toolStripButton3 As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripButton1 As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripButton2 As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripButton4 As System.Windows.Forms.ToolStripButton
    Private WithEvents lvResults As System.Windows.Forms.ListView
    Private WithEvents txtAbout As System.Windows.Forms.RichTextBox
    Private WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmdCancel As BIOLOGYAPPLICATION.FlatButton
    Friend WithEvents cmdSave As BIOLOGYAPPLICATION.FlatButton
End Class
