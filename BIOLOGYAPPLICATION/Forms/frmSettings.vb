Imports System.Data.OleDb

Public Class frmSettings

    Private Sub btnEditQuestion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If lvLessons.Items.Count > 0 Then
            Dim editLesson As New frmNewLesson

            editLesson.LessonDet(lvLessons.FocusedItem.Text)
            FillListView("SELECT lessonid, lessontitle, lessondescription, instructions FROM tbl_lessons", lvLessons)
        End If
    End Sub

    Private Sub frmSettings_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FillListView("SELECT lessonid, lessontitle, lessondescription, instructions FROM tbl_lessons", lvLessons)
        FillListView("SELECT a.questionid, a.questiontitle, b.lessontitle FROM tbl_questions a INNER JOIN tbl_lessons b ON b.lessonid=a.lessonid", lvQuestions)
    End Sub

    Private Sub btnAddNewlesson_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim addNew As New frmNewLesson
        addNew.ShowForm()
        FillListView("SELECT lessonid, lessontitle, lessondescription, instructions FROM tbl_lessons", lvLessons)
    End Sub

    Private Sub btnDeleteLessons_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDeleteLessons.Click
        Dim con As New OleDbConnection(DB_CONNECTION_STRING)
        con.Open()

        Dim cmd As New OleDbCommand("DELETE * FROM tbl_lessons WHERE lessonid=" & CInt(lvLessons.FocusedItem.SubItems(0).Text) & "", con)
        cmd.ExecuteNonQuery()
        MessageBox.Show("Record has been deleted")

        FillListView("SELECT lessonid, lessontitle, lessondescription, instructions FROM tbl_lessons", lvLessons)

        con.Close()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim addNew As New frmNewQ
        addNew.ShowForm()
        FillListView("SELECT a.questionid, a.questiontitle, b.lessontitle FROM tbl_questions a INNER JOIN tbl_lessons b ON b.lessonid=a.lessonid", lvQuestions)
    End Sub

    Private Sub cmdClose_Click(sender As Object, e As EventArgs) Handles cmdClose.Click
        Me.Close()
    End Sub

    Private Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        txtAbout.Text = String.Empty
    End Sub

    Private Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        Try
            ExecuteQuery("UPDATE tbl_about SET about = '" & txtAbout.ToString().Replace("'", " ") & "'  WHERE aboutid = 1")
        Catch generatedExceptionName As Exception
            MessageBox.Show("Invalid Characters", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
End Class
