Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data.OleDb
Imports System.Collections
Imports System.Globalization
Imports System.Windows.Forms
Imports System.Data
Imports System.Drawing
Imports BIOLOGYAPPLICATION.RSFLib

Namespace RSFLib
	Public NotInheritable Class RSFCommon
		Private Sub New()
		End Sub
		''' <summary>
		''' grouping of listview records
		''' </summary>
		''' <param name="lvw"></param>
		''' <param name="sql"></param>
		''' <param name="index"></param>
		Public Shared Sub groupRecs(lvw As ListView, sql As String, index As Integer, reader As OleDbDataReader)
			Dim menus As New List(Of ListViewGroup)()
			menus = RSFCommon.showGroups(sql, reader)
			For Each menu As ListViewGroup In menus
				lvw.Groups.AddRange(New ListViewGroup() {menu})
				For Each item As ListViewItem In lvw.Items
					Dim subItemText As String = item.SubItems(index).Text
					If menu.ToString() = subItemText Then
						item.Group = menu
						'TODO color to dues
						'TODO a random color here
						'TODO do a count of specific item list
						item.BackColor = System.Drawing.Color.WhiteSmoke
					End If
				Next
			Next
		End Sub

		''' <summary>
		''' show to a cbo
		''' </summary>
		''' <param name="sql"></param>
		''' <returns></returns>
		Public Shared Function showLists(sql As String, reader As OleDbDataReader) As ArrayList
			Dim str As New ArrayList()
			While reader.Read()
				str.Add(reader(0).ToString())
			End While
			Return str
		End Function
		Public Shared Function showGroups(sql As String, reader As OleDbDataReader) As List(Of ListViewGroup)
			Dim str As New List(Of ListViewGroup)()
			While reader.Read()
				str.Add(New ListViewGroup(reader(0).ToString()))
			End While
			Return str
		End Function

		''' <summary>
		''' get the first column in lvw
		''' </summary>
		''' <param name="lvw"></param>
		''' <returns></returns>
		Public Shared Function getTextFromLvw(lvw As System.Windows.Forms.ListView) As String
			Dim str As String = String.Empty
			For i As Integer = 0 To lvw.Items.Count - 1
				If lvw.Items(i).Selected = True Then
					str = lvw.Items(i).SubItems(1).Text
					Exit For
				End If
			Next
			Return str
		End Function
	End Class
End Namespace
