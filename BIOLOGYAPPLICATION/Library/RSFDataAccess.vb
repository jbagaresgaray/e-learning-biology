Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data.OleDb
Imports System.Windows.Forms
Imports System.Data
Imports System.Diagnostics

Namespace RSFLib
	Public Class RSFDataAccess
		Implements IDisposable
		Private _connection As OleDbConnection

		Public Sub New()
			_connection = InitConnection()
		End Sub

		Public Function InitConnection() As OleDbConnection
            Dim myConnection As New OleDbConnection(DB_CONNECTION_STRING)
			Try
				If myConnection.State = ConnectionState.Closed Then
					myConnection.Open()
				End If
			Catch e As System.Exception
				Console.WriteLine("Could not connect to Database, " & vbLf & vbCr & "Error :" & e.Message)
				Application.Restart()


				Process.GetCurrentProcess().Kill()
					'myConnection.Close();

			Finally
			End Try
			Return myConnection
		End Function

		Public Sub Dispose() Implements IDisposable.Dispose
			_connection.Dispose()
		End Sub

		''' <summary>
		''' DataSet
		''' </summary>
		''' <param name="query"></param>
		''' <param name="srcTable"></param>
		''' <returns>DataSet</returns>
		Public Function ds(query As String, srcTable As String) As DataSet
			Using da As New OleDbDataAdapter(query, _connection)
                Dim dd As New DataSet()
                da.Fill(dd, srcTable)
				Return ds
			End Using
		End Function
		''' <summary>
		''' Check if a record exist
		''' </summary>
		''' <param name="myQuery"></param>
		''' <returns>int</returns>
		Public Function recordExist(myQuery As String) As Integer
			Dim result As Integer = 0
			Using com As New OleDbCommand(myQuery, _connection)
				Dim count As Integer = CInt(com.ExecuteScalar())
				result = count
			End Using
			Return result
		End Function
		''' <summary>
		''' Retrieve Data using a reader
		''' </summary>
		''' <param name="query"></param>
		''' <returns>OleDbDataReader</returns>
		Public Function read(query As String) As OleDbDataReader
			Using com As New OleDbCommand(query, _connection)
				Dim reader As OleDbDataReader = com.ExecuteReader()
				Return reader
			End Using
		End Function
		''' <summary>
		''' get id from sql
		''' </summary>
		''' <param name="sql"></param>
		''' <returns>integer</returns>
		Public Function getID(sql As String) As Integer
			Dim num As Integer = 0
			Using reader As OleDbDataReader = read(sql)
				While reader.Read()
					num = reader.GetInt32(0)
				End While
			End Using
			Return num
		End Function
		Public Function editForm(query As String, table As String, Id As Integer) As DataRowCollection
			Using dataset As DataSet = ds(query, table)
				Dim drc As DataRowCollection = dataset.Tables(table).Rows

				Return drc
			End Using
		End Function
		''' <summary>
		''' Insert, Update, Delete
		''' </summary>
		''' <param name="myQuery"></param>
		''' <returns></returns>
		Public Function IUD(myQuery As String) As Boolean
			Dim result As Boolean = False
			Using com As New OleDbCommand(myQuery, _connection)
				com.ExecuteNonQuery()
				com.Dispose()
				result = True
			End Using
			Return result
		End Function
		''' <summary>
		''' insert and return the last id inserted
		''' </summary>
		''' <param name="myQuery"></param>
		''' <param name="identityQuery"></param>
		''' <returns></returns>
		Public Function InsertAndreturnID(myQuery As String, identityQuery As String) As Integer
			Dim ID As Integer = 0
			Using com As New OleDbCommand(myQuery, _connection)
				com.ExecuteNonQuery()
				com.CommandText = identityQuery
				ID = CInt(com.ExecuteScalar())
			End Using
			Return ID
		End Function

	End Class
End Namespace
