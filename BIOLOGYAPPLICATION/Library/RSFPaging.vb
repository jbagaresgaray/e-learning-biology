Imports System.Collections.Generic
Imports System.Data
Imports System.Data.OleDb
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks

Namespace RSFLib
	Public Class RSFPaging
		Public Property _connection() As OleDbConnection
			Get
				Return m__connection
			End Get
			Set
				m__connection = Value
			End Set
		End Property
		Private m__connection As OleDbConnection

		Private countIndex As Integer = 1
		' do the counting
		Private pageCount As Double = 0F
		' page to count / do the counting also
		Private pageIndex As Double = 0F
		' index page
		Private maxRecord As Double = 0F
		' maximum records
		Private recordsInPage As Double = 0F
		' records that is currently paged
		Public Property recordsToPage() As Integer
			Get
				Return m_recordsToPage
			End Get
			Set
				m_recordsToPage = Value
			End Set
		End Property
		Private m_recordsToPage As Integer
		' value that is supplied to the paging
		Public Property btnNext() As Boolean
			Get
				Return m_btnNext
			End Get
			Set
				m_btnNext = Value
			End Set
		End Property
		Private m_btnNext As Boolean
		Public Property btnPrev() As Boolean
			Get
				Return m_btnPrev
			End Get
			Set
				m_btnPrev = Value
			End Set
		End Property
		Private m_btnPrev As Boolean
		Public Property btnLast() As Boolean
			Get
				Return m_btnLast
			End Get
			Set
				m_btnLast = Value
			End Set
		End Property
		Private m_btnLast As Boolean
		Public Property btnFirst() As Boolean
			Get
				Return m_btnFirst
			End Get
			Set
				m_btnFirst = Value
			End Set
		End Property
		Private m_btnFirst As Boolean
		''' <summary>
		''' page records
		''' </summary>
		''' <param name="query"></param>
		''' <param name="tbl"></param>
		''' <returns></returns>
		Public Function pagerecord(query As String, tbl As String) As DataSet
			Try
				Using rsfda As New RSFDataAccess()
					Me._connection = rsfda.InitConnection()

					Using ds As New DataSet()
						Using da As New OleDbDataAdapter(query, Me._connection)
							da.Fill(ds, CInt(Math.Truncate(Me.pageIndex)), CInt(Me.recordsToPage), tbl)
							' get the first wave of records
							Me.recordsInPage = ds.Tables(tbl).Rows.Count
							'get the collected count of records
							Using dsMC As New DataSet()
								' get the total count of records
								da.Fill(dsMC, tbl)
								'TODO: Refactor - not good
								Me.maxRecord = dsMC.Tables(tbl).Rows.Count
							End Using

							Me.recordsToPage = If(Me.recordsToPage <= 0, CInt(Math.Truncate(Me.maxRecord)), Me.recordsToPage)
							Me.pageCount = CInt(Math.Truncate(Me.maxRecord / Me.recordsToPage))
							'divide the maxrecord to the supplied record value
							If (Me.maxRecord Mod Me.recordsToPage) > 0 Then
								' get the remainder
								Me.pageCount += 1
							End If
							Me.disablePageBtn()
						End Using
						rsfda.Dispose()

						Return ds
					End Using

				End Using
					'
			Catch generatedExceptionName As Exception
			End Try

			Return Nothing
		End Function
		''' <summary>
		''' page last
		''' </summary>
		''' <returns></returns>
		Public Function pageLast() As Boolean
			Dim ret As Boolean = False
			Me.pageIndex = CInt(Math.Truncate(Me.pageCount))
			If CInt(Math.Truncate(Me.pageCount)) = CInt(Math.Truncate(Me.pageIndex)) Then
				Me.pageIndex = Me.recordsToPage * (Me.pageIndex - 1)
				Me.countIndex = CInt(Math.Truncate(Me.pageCount))
				ret = True
			End If
			Return ret
		End Function
		''' <summary>
		''' page first
		''' </summary>
		''' <returns></returns>
		Public Function pageFirst() As Boolean
			Dim ret As Boolean = False
			Me.pageIndex = 0
			If Me.pageIndex = 0 Then
				Me.countIndex = (Me.recordsToPage * CInt(Math.Truncate(Me.pageIndex))) + 1
				Me.pageCount -= CInt(Math.Truncate(Me.pageCount))
				ret = True
			End If
			Return ret
		End Function
		''' <summary>
		''' page next
		''' </summary>
		''' <returns></returns>
		Public Function pagenext() As Boolean
			Dim ret As Boolean = False
			If Me.pageIndex <> Me.pageCount Then
				If Me.countIndex >= 0 Then
					Me.countIndex += 1
				End If
				ret = True
			End If
			Me.pageIndex += Me.recordsToPage
			' sends the paged count to the start
			Return ret
		End Function
		''' <summary>
		''' page previous
		''' </summary>
		''' <returns></returns>
		Public Function pageprev() As Boolean
			Dim ret As Boolean = False
			If Me.countIndex >= 0 Then
				If Me.countIndex > 0 Then
					Me.countIndex -= 1
				End If
				ret = True
			End If
			Me.pageIndex -= Me.recordsToPage
			' sends the paged count to the start
			Return ret
		End Function
		''' <summary>
		''' displays info
		''' </summary>
		''' <returns></returns>
		Public Function displayInfo() As String
			' current page
			' total page
			Dim message As String = [String].Format("Page - {0} / {1} - {2}", CInt(Me.countIndex), CInt(Math.Truncate(Me.pageCount)), CInt(Math.Truncate(Me.recordsInPage)) & " Records")
			' total records
			Return message
		End Function
		''' <summary>
		''' disables the button
		''' </summary>
		Public Sub disablePageBtn()
			If CInt(Math.Truncate(Me.pageCount)) = Me.countIndex Then
				Me.btnNext = False
				Me.btnLast = False
			Else
				Me.btnNext = True
				Me.btnLast = True
			End If
			If Me.countIndex <= 1 Then
				Me.btnPrev = False
				Me.btnFirst = False
			Else
				Me.btnPrev = True
				Me.btnFirst = True
			End If
		End Sub
	End Class
End Namespace

