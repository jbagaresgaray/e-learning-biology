Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Collections
Imports System.Data.OleDb
Imports System.Windows.Forms
Imports System.Drawing

Namespace RSFLib
	Public Class RSFTreeView
		Public Class TreeViewObj
			Public Property parentTable() As String
				Get
					Return m_parentTable
				End Get
				Set
					m_parentTable = Value
				End Set
			End Property
			Private m_parentTable As String
			Public Property childTable() As String
				Get
					Return m_childTable
				End Get
				Set
					m_childTable = Value
				End Set
			End Property
			Private m_childTable As String
			Public Property il() As System.Windows.Forms.ImageList
				Get
					Return m_il
				End Get
				Set
					m_il = Value
				End Set
			End Property
			Private m_il As System.Windows.Forms.ImageList
			Public Property tv() As System.Windows.Forms.TreeView
				Get
					Return m_tv
				End Get
				Set
					m_tv = Value
				End Set
			End Property
			Private m_tv As System.Windows.Forms.TreeView
		End Class

		Private Shared parentList As New ArrayList()
		Private Shared childList As New ArrayList()

		Public Property connectionString() As String
			Get
				Return m_connectionString
			End Get
			Set
				m_connectionString = Value
			End Set
		End Property
		Private m_connectionString As String

		Public Sub loadTreeViewMenus(treeviewvo As TreeViewObj)
			Try
				Dim da As New RSFDataAccess()

				Dim c As OleDbDataReader = da.read(String.Format("SELECT parentid, parent, iconindex FROM {0}", treeviewvo.parentTable))
				Dim p As OleDbDataReader = da.read(String.Format("SELECT parentid, child, iconindex FROM {0}", treeviewvo.childTable))

				treeviewvo.tv.ImageList = treeviewvo.il
				treeviewvo.tv.Nodes.Clear()

				While c.Read()
					Dim str As String = c("parentid").ToString() & ":" & c("parent").ToString()
					parentList.Add(New TreeNode(str, Convert.ToInt32(c("iconindex")), Convert.ToInt32(c("iconindex"))))
				End While
				While p.Read()
					Dim str1 As String = p("parentid").ToString() & ":" & p("child").ToString()
					childList.Add(New TreeNode(str1, Convert.ToInt32(p("iconindex")), Convert.ToInt32(p("iconindex"))))
				End While

				Dim boldfont As New Font(treeviewvo.tv.Font, FontStyle.Bold)

				For Each parentnode As TreeNode In parentList
					Dim parent As String = parentnode.Text
					Dim node As New TreeNode(parent.Substring(parent.IndexOf(":") + 1), parentnode.ImageIndex, parentnode.ImageIndex)

					node.NodeFont = boldfont
					treeviewvo.tv.Nodes.Add(node)

					Dim parentKey As String = parent.Substring(0, parent.IndexOf(":"))
					For Each childnode As TreeNode In childList
						Dim child As String = childnode.Text
						Dim childKey As String = child.Substring(0, child.IndexOf(":"))
						If parentKey = childKey Then
							treeviewvo.tv.Nodes(parentList.IndexOf(parentnode)).Nodes.Add(New TreeNode(child.Substring(child.IndexOf(":") + 1), childnode.ImageIndex, childnode.ImageIndex))
						End If
					Next
					node.Text += String.Empty
				Next
				childList.Clear()
				parentList.Clear()
				treeviewvo.tv.ExpandAll()
					'
			Catch generatedExceptionName As Exception
			End Try
		End Sub
	End Class

	Public Class ParentNode
		Public Property myParentName() As String
			Get
				Return m_myParentName
			End Get
			Set
				m_myParentName = Value
			End Set
		End Property
		Private m_myParentName As String
		Public Sub New(parentName As String)
			Me.myParentName = parentName
		End Sub
	End Class

	Public Class ChildNode
		Public Property myChildName() As String
			Get
				Return m_myChildName
			End Get
			Set
				m_myChildName = Value
			End Set
		End Property
		Private m_myChildName As String
		Public Sub New(childName As String)
			Me.myChildName = childName
		End Sub
	End Class
End Namespace
