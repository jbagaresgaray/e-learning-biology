Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Reflection
Imports System.Text.RegularExpressions
Imports System.Globalization

Namespace RSFLib
	Public Class User
		Public Property ID() As Integer
			Get
				Return m_ID
			End Get
			Set
				m_ID = Value
			End Set
		End Property
		Private m_ID As Integer
		Public Property username() As String
			Get
				Return m_username
			End Get
			Set
				m_username = Value
			End Set
		End Property
		Private m_username As String
		Public Property password() As String
			Get
				Return m_password
			End Get
			Set
				m_password = Value
			End Set
		End Property
		Private m_password As String
		Public Property cPassword() As String
			Get
				Return m_cPassword
			End Get
			Set
				m_cPassword = Value
			End Set
		End Property
		Private m_cPassword As String
		Public Property fullname() As String
			Get
				Return m_fullname
			End Get
			Set
				m_fullname = Value
			End Set
		End Property
		Private m_fullname As String
		Public Property created() As DateTime
			Get
				Return m_created
			End Get
			Set
				m_created = Value
			End Set
		End Property
		Private m_created As DateTime
		Public Property modified() As DateTime
			Get
				Return m_modified
			End Get
			Set
				m_modified = Value
			End Set
		End Property
		Private m_modified As DateTime
		Public Property level() As Integer
			Get
				Return m_level
			End Get
			Set
				m_level = Value
			End Set
		End Property
		Private m_level As Integer
	End Class

	Public NotInheritable Class RSFValidation
		Private Sub New()
		End Sub

	End Class
End Namespace
