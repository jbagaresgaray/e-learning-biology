Imports System.Collections.Generic
Imports System.Data.OleDb
Imports System.IO
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks

Module modAbout

    Public Function GetAboutText() As String
        Dim ret As String = String.Empty
        Try
            Dim con As New OleDbConnection(DB_CONNECTION_STRING)
            con.Open()
            Dim query As String = "SELECT about FROM tbl_about WHERE aboutid = 1"
            Dim com As New OleDbCommand(query, con)
            Dim reader As OleDbDataReader = com.ExecuteReader
            reader.Read()
            If reader.HasRows Then
                ret = reader(0).ToString()
            Else
                ret = ""
            End If
            reader.Close()
            con.Close()
        Catch generatedExceptionName As Exception
        End Try
        Return ret
    End Function

    Public Function UpdateAbout(aboutText As String) As Boolean
        If ExecuteQuery("UPDATE tbl_about SET about = '" & aboutText.ToString().Replace("'", " ") & "'  WHERE aboutid = 1") Then
            UpdateAbout = True
        Else
            UpdateAbout = False
        End If
    End Function

End Module
