Imports System.Data.OleDb
Imports System.Data
Imports System.Globalization
Module modConnector
    Public CONFIG_INI_FILE As String        'the configuration file

    Public DEFAULT_DATABASE As String       'Database
    Public DB_CONNECTION_STRING As String   'DB Connection String
    Public DB_PASSWORD As String            'DB Password

    Public isLogin As Boolean

    Public Enum FormState
        addState = 1
        updateState = 0
    End Enum

    Public Structure USER_INFO
        Dim IDUSER As Integer
        Dim USERNAME As String
        Dim USERISADMIN As Boolean
        Dim uType As String
    End Structure

    Public CURRENTUSER As USER_INFO


    Public Function getlastid(ByVal tablename As String) As Integer

        Dim con As New OleDbConnection(DB_CONNECTION_STRING)
        con.Open()

        Dim myCommand As New OleDbCommand
        Dim MyDRlastid As OleDbDataReader

        Dim sql As String

        Dim lastid As Integer

        DEFAULT_DATABASE = System.AppDomain.CurrentDomain.BaseDirectory & "/Database.mdb; Jet OLEDB:Database Password= express"

        sql = "SELECT AUTO_INCREMENT FROM /Database.tables WHERE table_name = '" & tablename & "' AND table_schema = DATABASE( )"

        myCommand.Connection = con
        myCommand.CommandText = sql

        MyDRlastid = myCommand.ExecuteReader

        If MyDRlastid.Read = True Then
            lastid = MyDRlastid("AUTO_INCREMENT") & ""
            lastid = lastid - 1

        End If

        MyDRlastid.Close()
        myCommand.Dispose()

        Return lastid

        con.Close()

    End Function

    '======================================================
    ' This procedure will Prepare the connection to the database
    '======================================================
    Public Function prepareDBConnString() As Integer
        Dim con As New OleDbConnection
        Dim isOpen As Boolean
        Dim ANS As MsgBoxResult
        isOpen = False
        '----------------------------
        ' Reset
        '----------------------------
        Dim strServer As String
        Dim strDatabase As String
        Dim strUserID As String
        Dim strPassword As String
        Dim strPort As String

        '----------------------------
        ' Connection String
        '----------------------------
        On Error GoTo prepareDBConnString_Error

        Do Until isOpen = True
            'DB_CONNECTION_STRING = "Data Source=" & SQLite_Database & ";Version=3;"
            DEFAULT_DATABASE = System.AppDomain.CurrentDomain.BaseDirectory & "/Database.mdb; Jet OLEDB:Database Password= express"

            DB_CONNECTION_STRING = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & DEFAULT_DATABASE

            con.ConnectionString = DB_CONNECTION_STRING
            con.Open()
            isOpen = True
            con.Close()
        Loop
        prepareDBConnString = isOpen
        Exit Function

prepareDBConnString_Error:
        ANS = MsgBox("Error Number: " & Err.Number & vbCrLf & "Description: " & Err.Description & " in procedure prepareDBConnString of Module mod_Main", _
                       MsgBoxStyle.RetryCancel)
        If ANS = vbCancel Then
            prepareDBConnString = MsgBoxResult.Cancel
            End
        ElseIf ANS = vbRetry Then
            prepareDBConnString = MsgBoxResult.Retry
        End If

    End Function

    '======================================================
    ' This procedure will execute the Main Application
    '======================================================
    Public Sub Main()

        Application.EnableVisualStyles()                        ' This is already default on Visual Basic Application
        Application.SetCompatibleTextRenderingDefault(False)    ' This is already default on Visual Basic Application

        isLogin = False

        prepareDBConnString()

        Application.Run(New frmLogin())  ' Method use to execute run command on what form to display first on the Application

        If isLogin Then

            Application.Run(New Main())
        End If
    End Sub
End Module
