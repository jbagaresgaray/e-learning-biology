Imports System.Data.OleDb
Imports System.Reflection
Imports System.Text.RegularExpressions

Module modFunctions

    Public Sub warnexit(ByVal frmtoExit As Form)

        Dim result As DialogResult

        result = MessageBox.Show("Are You sure you want to Exit?", _
            "Warning on Exit", _
            MessageBoxButtons.YesNo)

        If result = DialogResult.No Then 'no
            Exit Sub
        ElseIf result = DialogResult.Yes Then 'yes
            frmtoExit.Close()
        End If

    End Sub

    Public Function GetString(query As String) As String
        Dim con As New OleDbConnection(DB_CONNECTION_STRING)
        con.Open()
        Dim str As String = String.Empty
        Dim com As New OleDbCommand(query, con)
        Dim reader As OleDbDataReader = com.ExecuteReader
        While reader.Read()
            str = reader(0).ToString()
        End While
        Return str
        con.Close()
    End Function

    Public Function ExecuteQuery(ByVal sSQL As String) As Boolean
        On Error GoTo err
        Dim con As New OleDbConnection(DB_CONNECTION_STRING)
        con.Open()
        Dim com As New OleDbCommand(sSQL, con)
        com.ExecuteNonQuery()
        ExecuteQuery = True
        con.Close()
        Exit Function
err:
        ExecuteQuery = False
        DisplayErrorMsg("modFunction", "ExecuteQuery", Err.Number, Err.Description)
    End Function
    ' ===========================================================
    '   Use to execute SQL Query Commands with return values
    ' ===========================================================
    Public Function ExecuteQryReturn(ByVal sSQL As String, identityQuery As String) As Integer
        On Error GoTo err
        Dim con As New OleDbConnection(DB_CONNECTION_STRING)
        con.Open()
        Dim com As New OleDbCommand(sSQL, con)
        com.CommandText = identityQuery
        ExecuteQryReturn = CStr(com.ExecuteScalar())
        con.Close()
        Exit Function
err:
        ExecuteQryReturn = ""
    End Function
    ' ===========================================================
    '   Use for record checking , duplicate data checking
    ' ===========================================================
    Public Function QueryHasRows(ByVal sSQL As String) As Boolean
        Dim con As New OleDbConnection(DB_CONNECTION_STRING)
        con.Open()
        Dim com As New OleDbCommand(sSQL, con)
        Dim vRS As OleDbDataReader = com.ExecuteReader()
        vRS.Read()
        If vRS.HasRows Then
            QueryHasRows = True
        Else
            QueryHasRows = False
        End If
        vRS.Close()
        con.Close()
    End Function
    Public Sub FillListView(ByVal sSQL As String, ByRef lvList As ListView)

        Dim con As New OleDbConnection(DB_CONNECTION_STRING)
        con.Open()

        Dim record As Integer
        Dim com As New OleDbCommand(sSQL, con)
        Dim dr As OleDbDataReader = com.ExecuteReader

        lvList.Items.Clear()

        Do While dr.Read

            Dim lv As New ListViewItem(dr(0).ToString())

            For record = 1 To dr.FieldCount - 1
                lv.SubItems.Add(dr.Item(record).ToString())
            Next

            lvList.Items.AddRange(New ListViewItem() {lv})
        Loop

        dr.Close()
    End Sub

    Public Sub fillCombo(ByVal cbo As ComboBox, ByVal items As String)
        Dim con As New OleDbConnection(DB_CONNECTION_STRING)
        con.Open()

        Dim com As New OleDbCommand(items, con)
        Dim dr As OleDbDataReader = com.ExecuteReader
        cbo.Items.Clear()
        While dr.Read
            cbo.Items.AddRange(New Object() {dr(0).ToString})
        End While
        dr.Close()
    End Sub

    ' ===========================================================
    '   GENERATE CUSTOM ERROR MESSAGE
    ' ===========================================================
    Public Function DisplayErrorMsg(ByVal pModule As String, ByVal pProcedure As String, ByVal ErrorNbr As Long, ByVal ErrorDesc As String)

        Dim strErrorMsg As String

        On Error Resume Next

        strErrorMsg = "------------------------------------------------------------------------------------------------------------------------------------------" & vbNewLine & _
                      "Error          : " & ErrorNbr & vbNewLine & _
                      "Description: " & ErrorDesc & vbNewLine & _
                      "------------------------------------------------------------------------------------------------------------------------------------------" & vbNewLine & vbNewLine & _
                      "Module       : " & pModule & vbNewLine & _
                      "Procedure: " & pProcedure & vbNewLine & vbNewLine & _
                      "If this is the first time you saw this message, " & _
                      "kindly inform your Database Administrator" & vbNewLine & _
                      "or notify the Technical Support." & vbNewLine & vbNewLine & _
                      "Indicate the information shown in this dialog and what you were doing when this error occured." & vbNewLine & vbNewLine & _
                      "Thank you."

        MsgBox(strErrorMsg, vbCritical + vbOKOnly, "Error No. " & ErrorNbr)

    End Function

    Public Function isNumeric(str As String) As Boolean
        Return If(String.IsNullOrEmpty(str), False, Regex.IsMatch(str, "^\s*\-?\d+(\.\d+)?\s*$"))
    End Function

    Public Function confirmAction(msg As String, caption As String) As Boolean
        Dim result = MessageBox.Show(msg, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Return False
        End If
        Return True
    End Function

    Public Function toString(str As String) As String
        str = If(str = "", InlineAssignHelper(str, Nothing), str)
        Return str
    End Function

    Public Function toInt(num As Integer) As Integer
        num = If(num = 0, InlineAssignHelper(num, 0), num)
        Return num
    End Function

    Public Function toDecimal(str As String) As Decimal
        Dim dec As Decimal = Convert.ToDecimal(If(str = "", InlineAssignHelper(str, "0.00"), str))
        Return dec
    End Function

    Public Function validateObjField(vo As [Object]) As Boolean
        Dim t As Type = vo.[GetType]()
        For Each field As PropertyInfo In t.GetProperties()
            If field.GetValue(vo, Nothing) Is Nothing Then
                Return False
            ElseIf field.Equals(0) Then
                Return False
            End If
        Next
        Return True
    End Function
    Public Function validateObj(obj As [Object]) As Boolean
        If obj.Equals(String.Empty) Then
            Return False
        End If
        Return True
    End Function

    Public Function validateNum(obj As Decimal) As Boolean
        If obj = 0 Then
            Return True
        End If
        Return False
    End Function
    Private Function InlineAssignHelper(Of T)(ByRef target As T, value As T) As T
        target = value
        Return value
    End Function

    Public Sub PromptsMessage(index As Integer, Optional str As String = Nothing)
        Select Case index
            Case 1
                System.Windows.Forms.MessageBox.Show("Succesfully Inserted !", "Information", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information)
                Exit Select
            Case 2
                System.Windows.Forms.MessageBox.Show("Succesfully Updated !", "Information", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information)
                Exit Select
            Case 3
                System.Windows.Forms.MessageBox.Show("Succesfully Deleted !", "Information", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information)
                Exit Select
            Case 4
                System.Windows.Forms.MessageBox.Show("Record Already Exist, Please try a new one !", "Warning", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.[Stop])
                Exit Select
            Case 5
                System.Windows.Forms.MessageBox.Show("Select a record from the list!", "Note", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation)
                Exit Select
            Case 6
                System.Windows.Forms.MessageBox.Show("You have an invalid field/s, please try again ", "Notification", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.[Stop])
                Exit Select
            Case 7
                System.Windows.Forms.MessageBox.Show("Passwords dont match, please try again.. ", "Notification", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation)
                Exit Select
            Case 8
                System.Windows.Forms.MessageBox.Show("Please select a menu in the left side bar..  ", "Notification", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation)
                Exit Select
            Case 9
                System.Windows.Forms.MessageBox.Show(str & " is empty, please try again ", "Notification", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation)
                Exit Select
            Case Else
                Exit Select
        End Select
    End Sub

End Module
