Imports System.Data.OleDb
Imports BIOLOGYAPPLICATION.IIM.Entities

Module modLesson

    Public Structure LessonInfo
        Dim lessonID, userid As Integer
        Dim lessontitle, lessondescription, instructions, createdate, modifyDate, studentId As String
        Dim photo As Byte()
    End Structure

    Public Function DeleteLesson(lessonId As String) As Boolean
        DeleteLesson = False
        Try
            ExecuteQuery("DELETE FROM tbl_lessons WHERE lessonid =" & lessonId)
            DeleteLesson = True
        Catch generatedExceptionName As Exception
            DeleteLesson = False
        End Try
    End Function

    Public Function UpdateLesson(lesson As LessonInfo) As Boolean
        UpdateLesson = False
        Try
            Dim con As New OleDb.OleDbConnection(DB_CONNECTION_STRING)
            con.Open()

            Dim query As String = "UPDATE tbl_lessons SET lessontitle = '" & lesson.lessonTitle & "', lessondescription = '" & lesson.lessonDescription.ToString() & "', [instructions] = '" & lesson.instructions.ToString() & "', photo = @photo   WHERE lessonid =" & lesson.lessonId
            Dim com As New OleDbCommand(query, con)
            com.Parameters.AddWithValue("@photo", lesson.photo)
            com.ExecuteNonQuery()
            con.Close()
            UpdateLesson = True
        Catch generatedExceptionName As Exception
            UpdateLesson = False
        End Try
    End Function

    Public Function CreateNewLesson(lesson As LessonInfo) As Boolean

        CreateNewLesson = False
        Try
            Dim con As New OleDb.OleDbConnection(DB_CONNECTION_STRING)
            con.Open()
            Dim query As String = "INSERT INTO tbl_lessons (lessontitle, lessondescription, [instructions], photo) " & "VALUES( '" & lesson.lessontitle.ToString() & "','" & lesson.lessondescription.ToString() & "','" & lesson.instructions.ToString() & "', @photo)"
            Dim com As New OleDbCommand(query, con)
            com.Parameters.AddWithValue("@photo", lesson.photo)
            com.ExecuteNonQuery()
            con.Close()
            CreateNewLesson = True
        Catch generatedExceptionName As Exception
            CreateNewLesson = False
        End Try
    End Function

    Public Function GetAnswers(questionId As String) As List(Of Answer)
        Dim answers As New List(Of Answer)()
        Try
            Dim con As New OleDb.OleDbConnection(DB_CONNECTION_STRING)
            con.Open()
            Dim query As String = "SELECT answertext, questionid, iscorrect, answerid FROM qry_testquestions WHERE questionid =" & questionId
            Dim com As New OleDbCommand(query, con)
            Using reader As OleDbDataReader = com.ExecuteReader
                While reader.Read()
                    Dim answer As New Answer()
                    answer.AnswerText = reader(0).ToString()
                    answer.questionId = Convert.ToInt32(reader(1).ToString())
                    answer.isCorrect = Convert.ToBoolean(reader(2).ToString())
                    answer.answerId = Convert.ToInt32(reader(3).ToString())
                    answers.Add(answer)
                End While
            End Using
            con.Close()
        Catch generatedExceptionName As Exception
        End Try
        Return answers
    End Function

    Public Function LessonDetails(lessonId As String, ByRef lesson As LessonInfo) As Boolean
        LessonDetails = True
        Try
            Dim con As New OleDb.OleDbConnection(DB_CONNECTION_STRING)
            con.Open()
            Dim query As String = "SELECT  lessonid, lessontitle, lessondescription, [instructions], photo FROM qry_lessons WHERE lessonId =" & lessonId
            Dim com As New OleDbCommand(query, con)
            Using reader As OleDbDataReader = com.ExecuteReader
                reader.Read()
                If reader.HasRows Then
                    lesson.lessonId = Convert.ToInt32(reader(0).ToString())
                    lesson.lessonTitle = reader(1).ToString()
                    lesson.lessonDescription = reader(2).ToString()
                    lesson.instructions = reader(3).ToString()
                    lesson.photo = DirectCast(reader(4), Byte())

                    LessonDetails = True
                Else
                    LessonDetails = False
                End If
            End Using
            con.Close()
        Catch generatedExceptionName As Exception
            LessonDetails = False
        End Try
    End Function

    Public Function GetLessons() As List(Of LessonInfo)
        Dim list As New List(Of LessonInfo)()
        Try
            Dim con As New OleDb.OleDbConnection(DB_CONNECTION_STRING)
            con.Open()
            Dim query As String = "SELECT lessonid, lessontitle, lessondescription, [instructions], photo FROM qry_lessons"
            Dim com As New OleDbCommand(query, con)
            Using reader As OleDbDataReader = com.ExecuteReader
                While reader.Read()
                    Dim lesson As New LessonInfo()
                    lesson.lessonID = Convert.ToInt32(reader(0).ToString())
                    lesson.lessontitle = reader(1).ToString()
                    lesson.lessondescription = reader(2).ToString()
                    lesson.instructions = reader(3).ToString()
                    lesson.photo = DirectCast(reader(4), Byte())
                    list.Add(lesson)
                End While
            End Using
            con.Close()
        Catch generatedExceptionName As Exception
        End Try
        Return list.GroupBy(Function(test) test.lessonTitle).[Select](Function(grp) grp.First()).ToList()
    End Function

    Public Function GetUserLessons(userid As String) As List(Of LessonInfo)
        Dim list As New List(Of LessonInfo)()
        Try
            Dim con As New OleDb.OleDbConnection(DB_CONNECTION_STRING)
            con.Open()
            Dim query As String = "SELECT lessonid, lessontitle, lessondescription, userid, [instructions], photo FROM qry_userlessons WHERE userid =" & userid
            Dim com As New OleDbCommand(query, con)
            Using reader As OleDbDataReader = com.ExecuteReader
                While reader.Read()
                    Dim lesson As New LessonInfo()
                    lesson.lessonId = Convert.ToInt32(reader(0).ToString())
                    lesson.lessonTitle = reader(1).ToString()
                    lesson.lessonDescription = reader(2).ToString()
                    lesson.studentId = reader(3).ToString()
                    lesson.instructions = reader(4).ToString()
                    lesson.photo = DirectCast(reader(5), Byte())
                    list.Add(lesson)
                End While
            End Using
            con.Close()
        Catch generatedExceptionName As Exception
        End Try
        Return list.GroupBy(Function(test) test.lessonTitle).[Select](Function(grp) grp.First()).ToList()
    End Function

    Public Function GetAdminLessons() As List(Of LessonInfo)
        Dim list As New List(Of LessonInfo)()
        Try
            Dim con As New OleDb.OleDbConnection(DB_CONNECTION_STRING)
            con.Open()
            Dim query As String = "SELECT lessonid, lessontitle, lessondescription, userid, [instructions], photo FROM qry_userlessons"
            Dim com As New OleDbCommand(query, con)
            Using reader As OleDbDataReader = com.ExecuteReader
                While reader.Read()
                    Dim lesson As New LessonInfo()
                    lesson.lessonId = Convert.ToInt32(reader(0).ToString())
                    lesson.lessonTitle = reader(1).ToString()
                    lesson.lessonDescription = reader(2).ToString()
                    lesson.studentId = reader(3).ToString()
                    lesson.instructions = reader(4).ToString()
                    lesson.photo = DirectCast(reader(5), Byte())
                    list.Add(lesson)
                End While
            End Using
            con.Close()
        Catch generatedExceptionName As Exception
        End Try
        Return list.GroupBy(Function(test) test.lessonTitle).[Select](Function(grp) grp.First()).ToList()
    End Function
End Module
