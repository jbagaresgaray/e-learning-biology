Imports System.Collections.Generic
Imports System.Data.OleDb
Imports System.IO
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks

Module modQuestions

    Public Structure QuestionsInfo
        Dim testQuestionId, questionid, lessonid As Integer
        Dim questiontitle, createdate, modifydate, userid, answerImage As String
        Dim Answers As List(Of String)
    End Structure

    Public Structure Answer
        Dim answerid, questionid As Integer
        Dim answertext, answer_image, createdate, modifydate, userid As String
        Dim iscorrect As Boolean
    End Structure

    Public Function DeleteQuestion(questionId As String) As Boolean
        If ExecuteQuery("DELETE FROM tbl_questions WHERE questionid =" & questionId) Then
            DeleteQuestion = True
        Else
            DeleteQuestion = False
        End If
    End Function

    Public Function GetQuestion(questionid As String) As QuestionsInfo
        On Error GoTo err
        Dim question As New QuestionsInfo()
        Dim con As New OleDbConnection(DB_CONNECTION_STRING)
        con.Open()
        Dim query As String = "SELECT questiontitle, lessonid FROM qry_testquestions WHERE questionid =" & questionid
        Dim com As New OleDbCommand(query, con)
        Dim reader As OleDbDataReader = com.ExecuteReader
        While reader.Read()
            question.questiontitle = reader(0).ToString()
            question.lessonid = Convert.ToInt32(reader(1).ToString())
        End While
        reader.Close()
        con.Close()
        Return question
        Exit Function
err:
        DisplayErrorMsg("modQuestion", "GetQuestion", Err.Number, Err.Description)
    End Function

    Public Function UpdateQuestion(question As QuestionsInfo, Answer__1 As List(Of Answer)) As Integer
        UpdateQuestion= False

        If ExecuteQuery("UPDATE tbl_questions SET questiontitle = '" & question.questiontitle & "', lessonid = '" & question.lessonid & "'  WHERE questionid =" & question.questionid) Then
            If Answer__1.Count > 0 Then
                For Each answer__2 As Answer In Answer__1
                    ExecuteQuery("UPDATE tbl_answers SET answertext = '" & answer__2.answertext & "', iscorrect = " & answer__2.iscorrect & " WHERE answerid = " & answer__2.answerid)
                Next
            End If
        Else
            UpdateQuestion = False
        End If
    End Function

    Public Function GetQuestions(lessonId As String, ByRef questions As List(Of QuestionsInfo)) As Boolean
        GetQuestions = False
        Try
            Dim con As New OleDbConnection(DB_CONNECTION_STRING)
            con.Open()
            Dim qquery As String = "SELECT DISTINCT questiontitle, questionid FROM qry_testquestions WHERE lessonId =" & lessonId
            Dim com As New OleDbCommand(qquery, con)
            Dim qreader As OleDbDataReader = com.ExecuteReader
            While qreader.Read()
                Dim question As New QuestionsInfo()
                question.questiontitle = qreader(0).ToString()
                question.questionid = Convert.ToInt32(qreader(1).ToString())
                questions.Add(question)
            End While
            con.Close()
            GetQuestions = True
        Catch generatedExceptionName As Exception
            GetQuestions = False
        End Try
    End Function

    Public Function CreateNewQuestion(question As QuestionsInfo, Answers As List(Of Answer)) As Boolean
        Dim ret As Integer = 0
        CreateNewQuestion = False
        Try
            Dim query As String = "INSERT INTO tbl_questions (questiontitle, lessonid) " & "VALUES( '" & question.questiontitle & "','" & question.lessonid & "')"
            Dim identityQuery As String = "Select @@Identity"
            ret = ExecuteQryReturn(query.ToString(), identityQuery.ToString())
            If ret > 0 Then
                If Answers.Count > 0 Then
                    For Each answer As Answer In Answers
                        If ExecuteQuery("INSERT INTO tbl_answers (answertext, questionid, iscorrect) " & "VALUES( '" & answer.answertext & "'," & ret & ", " & answer.iscorrect & " )") Then
                            CreateNewQuestion = True
                        End If
                    Next

                End If
            Else
                CreateNewQuestion = True
            End If
        Catch generatedExceptionName As Exception
            CreateNewQuestion = False
        End Try
    End Function

End Module
