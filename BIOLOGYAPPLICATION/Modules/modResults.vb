
Imports System.Collections.Generic
Imports System.Data.OleDb
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports BIOLOGYAPPLICATION.IIM.Entities

Module modResults

    Public Structure TestResults
        Dim testresultid As Integer
        Dim studentname, result, createdate As String
    End Structure

    Public Structure UserResults
        Dim userId As Integer
        Dim correctAnswers, wrongAnswers, result, createdate As String
    End Structure


    Public Function DeleteTestResults(testresultId As String) As Boolean
        DeleteTestResults = False
        Try
            ExecuteQuery("DELETE FROM tbl_testresults WHERE testresultid =" & testresultId)
            DeleteTestResults = True
        Catch generatedExceptionName As Exception
            DeleteTestResults = False
        End Try
    End Function

    Public Function GetTestResults(testresultId As String) As List(Of TestResults)
        Dim testResults As New List(Of TestResults)()
        Try
            Dim con As New OleDbConnection(DB_CONNECTION_STRING)
            con.Open()
            Dim query As String = "SELECT studentname, [result], [createdate] testresultid FROM tbl_testresults WHERE testresultid =" & testresultId
            Dim com As New OleDbCommand(query, con)
            Dim reader As OleDbDataReader = com.ExecuteReader
            While reader.Read()
                Dim testResult As New TestResults()
                testResult.studentname = reader(0).ToString()
                testResult.result = reader(1).ToString()
                testResult.createdate = reader(2).ToString()
                testResult.testresultid = Convert.ToInt32(reader(1).ToString())
                testResults.Add(testResult)
            End While
            con.Close()
        Catch generatedExceptionName As Exception
        End Try
        Return testResults
    End Function

    Public Function InsertResultsToDatabse(result As TestResults) As Boolean
        If ExecuteQuery("INSERT INTO tbl_testresults (studentname, [result], [createdate]) " & "VALUES( '" & result.studentname.ToString() & "', '" & result.result.ToString() & "', '" & result.createdate.ToString() & "')") Then
            InsertResultsToDatabse = True
        Else
            InsertResultsToDatabse = False
        End If
    End Function
    Public Function CollectUserResults(userresults As UserResults) As Boolean
        If ExecuteQuery("INSERT INTO tbl_userresults (userid, result) " & "VALUES( " & userresults.userId & ", '" & userresults.result.ToString().Replace(vbCr & vbLf, " ").Replace("-", "") & "')") Then
            CollectUserResults = True
        Else
            CollectUserResults = False
        End If
    End Function

    Public Function GetQuizUser(username As String, password As String) As User
        Dim user As New User()
        Try
            Dim con As New OleDbConnection(DB_CONNECTION_STRING)
            con.Open()
            Dim query As String = "SELECT username, password, userid, userlevel FROM tbl_users WHERE username ='" & username & "' AND password = '" & password & "'"
            Dim com As New OleDbCommand(query, con)
            Dim reader As OleDbDataReader = com.ExecuteReader
            reader.Read()
            If reader.HasRows Then
                user.username = reader(0).ToString()
                user.password = reader(1).ToString()
                user.userid = Convert.ToInt32(reader(2).ToString())
                user.userlevel = Convert.ToInt32(reader(3).ToString())
            End If
            reader.Close()
            con.Close()
        Catch generatedExceptionName As Exception
        End Try
        Return user
    End Function
End Module
