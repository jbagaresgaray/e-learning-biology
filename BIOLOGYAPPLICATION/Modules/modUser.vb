Imports System.Collections.Generic
Imports System.Data.OleDb
Imports System.IO
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports BIOLOGYAPPLICATION.IIM.Entities

Module modUser

    Public Structure User
        Dim userid, userlevel As Integer
        Dim username, password, createdate, modifydate As String
    End Structure

    Public Structure UserLesson
        Dim userLessonId, groupid, lessonid, userid, userLevel As Integer
        Dim groupName, username, password As String
        Dim lessonIds As List(Of String)
    End Structure


    Public Function DeleteUser(userId As String) As Boolean
        If ExecuteQuery("DELETE FROM tbl_users WHERE userid =" & userId) Then
            DeleteUser = True
        Else
            DeleteUser = False
        End If
    End Function

    Public Function GetUserLesson(userId As Integer) As List(Of LessonInfo)
        Dim list As New List(Of LessonInfo)()
        Try
            Dim con As New OleDbConnection(DB_CONNECTION_STRING)
            con.Open()
            Dim query As String = "SELECT lessonid FROM qry_userlessons WHERE userid =" & userId
            Dim com As New OleDbCommand(query, con)
            Dim reader As OleDbDataReader = com.ExecuteReader
            While reader.Read()
                Dim lesson As New LessonInfo()
                lesson.lessonID = Convert.ToInt32(reader(0).ToString())
                list.Add(lesson)
            End While
            con.Close()
        Catch generatedExceptionName As Exception
        End Try
        Return list
    End Function

    Public Function GetUser(userId As String) As User
        Dim user As New User()
        Try
            Dim con As New OleDbConnection(DB_CONNECTION_STRING)
            con.Open()
            Dim query As String = "SELECT username, password FROM tbl_users WHERE userid =" & userId
            Dim com As New OleDbCommand(query, con)
            Dim reader As OleDbDataReader = com.ExecuteReader
            reader.Read()
            If reader.HasRows Then
                user.username = reader(0).ToString()
                user.password = reader(1).ToString()
            End If
            reader.Close()
            con.Close()
        Catch generatedExceptionName As Exception
        End Try
        Return user
    End Function

    Public Function UpdateUserLesson(user As UserLesson, lessons As List(Of LessonInfo)) As Boolean
        Dim ret As Boolean = False
        Try
            If ExecuteQuery("UPDATE tbl_users SET username = '" & user.username & "', [password] = '" & user.password & "' WHERE userid =" & user.userid) Then
                ExecuteQuery("DELETE FROM tbl_userlessons WHERE userid =" & user.userid)
                For Each lesson As LessonInfo In lessons
                    Dim userInsertQuery As String = "INSERT INTO tbl_userlessons ( userid, lessonid) VALUES(" & user.userid & "," & lesson.lessonID & ")"
                    ExecuteQuery(userInsertQuery)
                Next
            End If
        Catch generatedExceptionName As Exception
        End Try
        Return ret
    End Function

    Public Function CreateNewUserLesson(user As UserLesson, lessons As List(Of LessonInfo)) As Boolean
        Dim ret As Boolean = False
        Try
            If QueryHasRows("SELECT * FROM tbl_users WHERE username LIKE '" & user.username & "'") Then
                Dim query As String = "INSERT INTO tbl_users ([username], [password], userlevel) " & "VALUES( '" & user.username & "','" & user.password & "', " & user.userLevel & ")"
                Dim identityQuery As String = "Select @@Identity"
                Dim id = ExecuteQryReturn(query.ToString(), identityQuery.ToString())

                ret = True

                If id > 0 Then
                    For Each lesson As LessonInfo In lessons
                        Dim userInsertQuery As String = "INSERT INTO tbl_userlessons ( userid, lessonid) VALUES(" & id & "," & lesson.lessonID & ")"
                        ExecuteQuery(userInsertQuery)
                        ret = True
                    Next
                End If

                Return ret
            End If
        Catch generatedExceptionName As Exception
        End Try
        Return ret
    End Function
End Module
