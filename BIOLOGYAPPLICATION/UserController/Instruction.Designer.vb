Partial Class UCTestInstruction
	''' <summary>
	''' Required designer variable.
	''' </summary>
	Private components As System.ComponentModel.IContainer = Nothing

	''' <summary>
	''' Clean up any resources being used.
	''' </summary>
	''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	Protected Overrides Sub Dispose(disposing As Boolean)
		If disposing AndAlso (components IsNot Nothing) Then
			components.Dispose()
		End If
		MyBase.Dispose(disposing)
	End Sub

	#Region "Component Designer generated code"

	''' <summary>
	''' Required method for Designer support - do not modify
	''' the contents of this method with the code editor.
	''' </summary>
	Private Sub InitializeComponent()
		Me.txtInstruction = New System.Windows.Forms.TextBox()
		Me.btnStart = New System.Windows.Forms.Button()
		Me.btnCancelTest = New System.Windows.Forms.Button()
		Me.panel1 = New System.Windows.Forms.Panel()
		Me.textBox1 = New System.Windows.Forms.TextBox()
		Me.panel2 = New System.Windows.Forms.Panel()
		Me.panel1.SuspendLayout()
		Me.panel2.SuspendLayout()
		Me.SuspendLayout()
		'
		' txtInstruction
		'
		Me.txtInstruction.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.txtInstruction.Dock = System.Windows.Forms.DockStyle.Fill
		Me.txtInstruction.Font = New System.Drawing.Font("Georgia", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
		Me.txtInstruction.Location = New System.Drawing.Point(0, 36)
		Me.txtInstruction.Margin = New System.Windows.Forms.Padding(10)
		Me.txtInstruction.Multiline = True
		Me.txtInstruction.Name = "txtInstruction"
		Me.txtInstruction.[ReadOnly] = True
		Me.txtInstruction.Size = New System.Drawing.Size(723, 343)
		Me.txtInstruction.TabIndex = 0
		'
		' btnStart
		'
		Me.btnStart.Cursor = System.Windows.Forms.Cursors.Hand
		Me.btnStart.Font = New System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
		Me.btnStart.Location = New System.Drawing.Point(8, 3)
		Me.btnStart.Name = "btnStart"
		Me.btnStart.Size = New System.Drawing.Size(76, 25)
		Me.btnStart.TabIndex = 1
		Me.btnStart.Text = "Start Test"
		Me.btnStart.UseVisualStyleBackColor = True
		AddHandler Me.btnStart.Click, New System.EventHandler(AddressOf Me.btnStart_Click)
		'
		' btnCancelTest
		'
		Me.btnCancelTest.Cursor = System.Windows.Forms.Cursors.Hand
		Me.btnCancelTest.Font = New System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
		Me.btnCancelTest.Location = New System.Drawing.Point(84, 3)
		Me.btnCancelTest.Name = "btnCancelTest"
		Me.btnCancelTest.Size = New System.Drawing.Size(76, 25)
		Me.btnCancelTest.TabIndex = 2
		Me.btnCancelTest.Text = "Cancel Test"
		Me.btnCancelTest.UseVisualStyleBackColor = True
		AddHandler Me.btnCancelTest.Click, New System.EventHandler(AddressOf Me.btnCancelTest_Click)
		'
		' panel1
		'
		Me.panel1.BackColor = System.Drawing.Color.DarkOliveGreen
		Me.panel1.Controls.Add(Me.textBox1)
		Me.panel1.Dock = System.Windows.Forms.DockStyle.Top
		Me.panel1.Location = New System.Drawing.Point(0, 0)
		Me.panel1.Name = "panel1"
		Me.panel1.Size = New System.Drawing.Size(723, 36)
		Me.panel1.TabIndex = 3
		'
		' textBox1
		'
		Me.textBox1.BackColor = System.Drawing.Color.DarkOliveGreen
		Me.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.textBox1.Dock = System.Windows.Forms.DockStyle.Top
		Me.textBox1.Font = New System.Drawing.Font("Georgia", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
		Me.textBox1.ForeColor = System.Drawing.SystemColors.MenuBar
		Me.textBox1.Location = New System.Drawing.Point(0, 0)
		Me.textBox1.Margin = New System.Windows.Forms.Padding(0)
		Me.textBox1.Name = "textBox1"
		Me.textBox1.Size = New System.Drawing.Size(723, 33)
		Me.textBox1.TabIndex = 4
		Me.textBox1.Text = "Instructions"
		Me.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
		'
		' panel2
		'
		Me.panel2.Controls.Add(Me.btnCancelTest)
		Me.panel2.Controls.Add(Me.btnStart)
		Me.panel2.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.panel2.Location = New System.Drawing.Point(0, 379)
		Me.panel2.Name = "panel2"
		Me.panel2.Size = New System.Drawing.Size(723, 32)
		Me.panel2.TabIndex = 4
		'
		' UCTestInstruction
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6F, 13F)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.AutoScroll = True
		Me.BackColor = System.Drawing.SystemColors.ButtonHighlight
		Me.Controls.Add(Me.txtInstruction)
		Me.Controls.Add(Me.panel1)
		Me.Controls.Add(Me.panel2)
		Me.Name = "UCTestInstruction"
		Me.Size = New System.Drawing.Size(723, 411)
		Me.panel1.ResumeLayout(False)
		Me.panel1.PerformLayout()
		Me.panel2.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

	#End Region

	Public txtInstruction As System.Windows.Forms.TextBox
	Public btnStart As System.Windows.Forms.Button
	Public btnCancelTest As System.Windows.Forms.Button
	Private panel1 As System.Windows.Forms.Panel
	Private textBox1 As System.Windows.Forms.TextBox
	Private panel2 As System.Windows.Forms.Panel

End Class
