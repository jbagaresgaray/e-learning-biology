Namespace USERCONTROLS
	Partial Class UCMainContent
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Component Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
            Me.rTxtAbout = New System.Windows.Forms.RichTextBox()
            Me.pictureBox1 = New System.Windows.Forms.PictureBox()
            CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'rTxtAbout
            '
            Me.rTxtAbout.BackColor = System.Drawing.SystemColors.Control
            Me.rTxtAbout.BorderStyle = System.Windows.Forms.BorderStyle.None
            Me.rTxtAbout.Dock = System.Windows.Forms.DockStyle.Fill
            Me.rTxtAbout.Font = New System.Drawing.Font("Georgia", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.rTxtAbout.Location = New System.Drawing.Point(0, 0)
            Me.rTxtAbout.Margin = New System.Windows.Forms.Padding(10)
            Me.rTxtAbout.Name = "rTxtAbout"
            Me.rTxtAbout.ReadOnly = True
            Me.rTxtAbout.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
            Me.rTxtAbout.Size = New System.Drawing.Size(769, 444)
            Me.rTxtAbout.TabIndex = 3
            Me.rTxtAbout.TabStop = False
            Me.rTxtAbout.Text = ""
            '
            'pictureBox1
            '
            Me.pictureBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight
            Me.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
            Me.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.pictureBox1.Location = New System.Drawing.Point(0, 0)
            Me.pictureBox1.Name = "pictureBox1"
            Me.pictureBox1.Size = New System.Drawing.Size(769, 444)
            Me.pictureBox1.TabIndex = 0
            Me.pictureBox1.TabStop = False
            '
            'UCMainContent
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.AutoScroll = True
            Me.BackColor = System.Drawing.SystemColors.ActiveBorder
            Me.Controls.Add(Me.rTxtAbout)
            Me.Controls.Add(Me.pictureBox1)
            Me.Name = "UCMainContent"
            Me.Size = New System.Drawing.Size(769, 444)
            CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

		#End Region

		Private pictureBox1 As System.Windows.Forms.PictureBox
        Public rTxtAbout As System.Windows.Forms.RichTextBox

	End Class
End Namespace
