Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Drawing
Imports System.Data
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Windows.Forms

Namespace USERCONTROLS
    Partial Public Class UCMainContent
        Inherits UserControl

        Public Sub New()
            InitializeComponent()
        End Sub

        Private Sub UCMainContent_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            rTxtAbout.Text = GetAboutText()
        End Sub
    End Class
End Namespace
