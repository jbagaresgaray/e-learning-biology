Namespace IIM.USERCONTROLS
	Partial Class Navigation
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Component Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
			Me.flowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
			Me.SuspendLayout()
			'
			' flowLayoutPanel1
			'
			Me.flowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
			Me.flowLayoutPanel1.Name = "flowLayoutPanel1"
			Me.flowLayoutPanel1.Size = New System.Drawing.Size(248, 641)
			Me.flowLayoutPanel1.TabIndex = 0
			'
			' Navigation
			'
			Me.AutoScaleDimensions = New System.Drawing.SizeF(6F, 13F)
			Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
			Me.Controls.Add(Me.flowLayoutPanel1)
			Me.Name = "Navigation"
			Me.Size = New System.Drawing.Size(254, 644)
			Me.ResumeLayout(False)

		End Sub

		#End Region

		Private flowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
	End Class
End Namespace
