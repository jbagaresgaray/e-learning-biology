Namespace USERCONTROLS
	Partial Class UCTestResult
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Component Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
			Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(UCTestResult))
			Me.panel1 = New System.Windows.Forms.Panel()
			Me.label2 = New System.Windows.Forms.Label()
			Me.txtCorrectAnswer = New System.Windows.Forms.TextBox()
			Me.txtWrongAnswer = New System.Windows.Forms.TextBox()
			Me.btnBackToLessons = New System.Windows.Forms.Button()
			Me.lblCorrectAnswer = New System.Windows.Forms.Label()
			Me.lblWrongAnswers = New System.Windows.Forms.Label()
			Me.lblResults = New System.Windows.Forms.Label()
			Me.RTResults = New System.Windows.Forms.RichTextBox()
			Me.btnPrint = New System.Windows.Forms.Button()
			Me.pageSetupDialog1 = New System.Windows.Forms.PageSetupDialog()
			Me.printDocument1 = New System.Drawing.Printing.PrintDocument()
			Me.printDialog1 = New System.Windows.Forms.PrintDialog()
			Me.printPreviewDialog1 = New System.Windows.Forms.PrintPreviewDialog()
			Me.panel2 = New System.Windows.Forms.Panel()
			Me.panel3 = New System.Windows.Forms.Panel()
			Me.panel4 = New System.Windows.Forms.Panel()
			Me.panel1.SuspendLayout()
			Me.panel2.SuspendLayout()
			Me.panel3.SuspendLayout()
			Me.panel4.SuspendLayout()
			Me.SuspendLayout()
			'
			' panel1
			'
			Me.panel1.BackColor = System.Drawing.Color.DarkOliveGreen
			Me.panel1.Controls.Add(Me.label2)
			Me.panel1.Dock = System.Windows.Forms.DockStyle.Top
			Me.panel1.Location = New System.Drawing.Point(0, 0)
			Me.panel1.Name = "panel1"
			Me.panel1.Size = New System.Drawing.Size(619, 59)
			Me.panel1.TabIndex = 0
			'
			' label2
			'
			Me.label2.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.label2.AutoSize = True
			Me.label2.Font = New System.Drawing.Font("Georgia", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
			Me.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight
			Me.label2.Location = New System.Drawing.Point(212, 21)
			Me.label2.Name = "label2"
			Me.label2.Size = New System.Drawing.Size(281, 29)
			Me.label2.TabIndex = 5
			Me.label2.Text = "YOUR TEST RESULT IS:"
			'
			' txtCorrectAnswer
			'
			Me.txtCorrectAnswer.BackColor = System.Drawing.SystemColors.Control
			Me.txtCorrectAnswer.BorderStyle = System.Windows.Forms.BorderStyle.None
			Me.txtCorrectAnswer.Font = New System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CByte(0))
			Me.txtCorrectAnswer.Location = New System.Drawing.Point(386, 11)
			Me.txtCorrectAnswer.Name = "txtCorrectAnswer"
			Me.txtCorrectAnswer.Size = New System.Drawing.Size(51, 23)
			Me.txtCorrectAnswer.TabIndex = 1
			'
			' txtWrongAnswer
			'
			Me.txtWrongAnswer.BackColor = System.Drawing.SystemColors.Control
			Me.txtWrongAnswer.BorderStyle = System.Windows.Forms.BorderStyle.None
			Me.txtWrongAnswer.Font = New System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CByte(0))
			Me.txtWrongAnswer.Location = New System.Drawing.Point(549, 7)
			Me.txtWrongAnswer.Name = "txtWrongAnswer"
			Me.txtWrongAnswer.Size = New System.Drawing.Size(51, 26)
			Me.txtWrongAnswer.TabIndex = 2
			AddHandler Me.txtWrongAnswer.TextChanged, New System.EventHandler(AddressOf Me.txtWrongAnswer_TextChanged)
			'
			' btnBackToLessons
			'
			Me.btnBackToLessons.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
			Me.btnBackToLessons.Cursor = System.Windows.Forms.Cursors.Hand
			Me.btnBackToLessons.Font = New System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
			Me.btnBackToLessons.Location = New System.Drawing.Point(8, 5)
			Me.btnBackToLessons.Name = "btnBackToLessons"
			Me.btnBackToLessons.Size = New System.Drawing.Size(111, 28)
			Me.btnBackToLessons.TabIndex = 3
			Me.btnBackToLessons.Text = "Back to Lessons"
			Me.btnBackToLessons.UseVisualStyleBackColor = True
			AddHandler Me.btnBackToLessons.Click, New System.EventHandler(AddressOf Me.btnBackToLessons_Click)
			'
			' lblCorrectAnswer
			'
			Me.lblCorrectAnswer.AutoSize = True
			Me.lblCorrectAnswer.Font = New System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CByte(0))
			Me.lblCorrectAnswer.ForeColor = System.Drawing.Color.DarkGreen
			Me.lblCorrectAnswer.Location = New System.Drawing.Point(265, 11)
			Me.lblCorrectAnswer.Name = "lblCorrectAnswer"
			Me.lblCorrectAnswer.Size = New System.Drawing.Size(115, 16)
			Me.lblCorrectAnswer.TabIndex = 4
			Me.lblCorrectAnswer.Text = "Correct Answers:"
			'
			' lblWrongAnswers
			'
			Me.lblWrongAnswers.AutoSize = True
			Me.lblWrongAnswers.Font = New System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CByte(0))
			Me.lblWrongAnswers.ForeColor = System.Drawing.Color.Maroon
			Me.lblWrongAnswers.Location = New System.Drawing.Point(443, 11)
			Me.lblWrongAnswers.Name = "lblWrongAnswers"
			Me.lblWrongAnswers.Size = New System.Drawing.Size(109, 16)
			Me.lblWrongAnswers.TabIndex = 5
			Me.lblWrongAnswers.Text = "Wrong Answers:"
			'
			' lblResults
			'
			Me.lblResults.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.lblResults.AutoSize = True
			Me.lblResults.Font = New System.Drawing.Font("Georgia", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
			Me.lblResults.Location = New System.Drawing.Point(224, -9)
			Me.lblResults.Name = "lblResults"
			Me.lblResults.Size = New System.Drawing.Size(228, 109)
			Me.lblResults.TabIndex = 6
			Me.lblResults.Text = "1/12"
			'
			' RTResults
			'
			Me.RTResults.Dock = System.Windows.Forms.DockStyle.Fill
			Me.RTResults.Font = New System.Drawing.Font("Georgia", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
			Me.RTResults.Location = New System.Drawing.Point(0, 0)
			Me.RTResults.Name = "RTResults"
			Me.RTResults.Size = New System.Drawing.Size(619, 264)
			Me.RTResults.TabIndex = 7
			Me.RTResults.Text = ""
			'
			' btnPrint
			'
			Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
			Me.btnPrint.Cursor = System.Windows.Forms.Cursors.Hand
			Me.btnPrint.Font = New System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
			Me.btnPrint.Location = New System.Drawing.Point(120, 5)
			Me.btnPrint.Name = "btnPrint"
			Me.btnPrint.Size = New System.Drawing.Size(80, 28)
			Me.btnPrint.TabIndex = 8
			Me.btnPrint.Text = "Print"
			Me.btnPrint.UseVisualStyleBackColor = True
			AddHandler Me.btnPrint.Click, New System.EventHandler(AddressOf Me.btnPrint_Click)
			'
			' pageSetupDialog1
			'
			Me.pageSetupDialog1.Document = Me.printDocument1
			'
			' printDocument1
			'
			AddHandler Me.printDocument1.BeginPrint, New System.Drawing.Printing.PrintEventHandler(AddressOf Me.OnBeginPrint)
			AddHandler Me.printDocument1.PrintPage, New System.Drawing.Printing.PrintPageEventHandler(AddressOf Me.OnPrintPage)
			'
			' printDialog1
			'
			Me.printDialog1.Document = Me.printDocument1
			'
			' printPreviewDialog1
			'
			Me.printPreviewDialog1.AutoScrollMargin = New System.Drawing.Size(0, 0)
			Me.printPreviewDialog1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
			Me.printPreviewDialog1.ClientSize = New System.Drawing.Size(400, 300)
			Me.printPreviewDialog1.Document = Me.printDocument1
			Me.printPreviewDialog1.Enabled = True
			Me.printPreviewDialog1.Icon = DirectCast(resources.GetObject("printPreviewDialog1.Icon"), System.Drawing.Icon)
			Me.printPreviewDialog1.Name = "printPreviewDialog1"
			Me.printPreviewDialog1.Visible = False
			'
			' panel2
			'
			Me.panel2.Controls.Add(Me.lblResults)
			Me.panel2.Dock = System.Windows.Forms.DockStyle.Top
			Me.panel2.Location = New System.Drawing.Point(0, 59)
			Me.panel2.Name = "panel2"
			Me.panel2.Size = New System.Drawing.Size(619, 100)
			Me.panel2.TabIndex = 9
			'
			' panel3
			'
			Me.panel3.Controls.Add(Me.RTResults)
			Me.panel3.Dock = System.Windows.Forms.DockStyle.Fill
			Me.panel3.Location = New System.Drawing.Point(0, 159)
			Me.panel3.Name = "panel3"
			Me.panel3.Size = New System.Drawing.Size(619, 264)
			Me.panel3.TabIndex = 10
			'
			' panel4
			'
			Me.panel4.Controls.Add(Me.btnBackToLessons)
			Me.panel4.Controls.Add(Me.txtCorrectAnswer)
			Me.panel4.Controls.Add(Me.btnPrint)
			Me.panel4.Controls.Add(Me.txtWrongAnswer)
			Me.panel4.Controls.Add(Me.lblCorrectAnswer)
			Me.panel4.Controls.Add(Me.lblWrongAnswers)
			Me.panel4.Dock = System.Windows.Forms.DockStyle.Bottom
			Me.panel4.Location = New System.Drawing.Point(0, 384)
			Me.panel4.Name = "panel4"
			Me.panel4.Size = New System.Drawing.Size(619, 39)
			Me.panel4.TabIndex = 11
			'
			' UCTestResult
			'
			Me.AutoScaleDimensions = New System.Drawing.SizeF(6F, 13F)
			Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
			Me.Controls.Add(Me.panel4)
			Me.Controls.Add(Me.panel3)
			Me.Controls.Add(Me.panel2)
			Me.Controls.Add(Me.panel1)
			Me.Name = "UCTestResult"
			Me.Size = New System.Drawing.Size(619, 423)
			Me.panel1.ResumeLayout(False)
			Me.panel1.PerformLayout()
			Me.panel2.ResumeLayout(False)
			Me.panel2.PerformLayout()
			Me.panel3.ResumeLayout(False)
			Me.panel4.ResumeLayout(False)
			Me.panel4.PerformLayout()
			Me.ResumeLayout(False)

		End Sub

		#End Region

		Private panel1 As System.Windows.Forms.Panel
		Private lblCorrectAnswer As System.Windows.Forms.Label
		Private lblWrongAnswers As System.Windows.Forms.Label
		Private label2 As System.Windows.Forms.Label
		Public lblResults As System.Windows.Forms.Label
		Public txtCorrectAnswer As System.Windows.Forms.TextBox
		Public txtWrongAnswer As System.Windows.Forms.TextBox
		Public btnBackToLessons As System.Windows.Forms.Button
		Public RTResults As System.Windows.Forms.RichTextBox
		Public btnPrint As System.Windows.Forms.Button
		Private pageSetupDialog1 As System.Windows.Forms.PageSetupDialog
		Private printDocument1 As System.Drawing.Printing.PrintDocument
		Private printDialog1 As System.Windows.Forms.PrintDialog
		Private printPreviewDialog1 As System.Windows.Forms.PrintPreviewDialog
		Private panel2 As System.Windows.Forms.Panel
		Private panel3 As System.Windows.Forms.Panel
		Private panel4 As System.Windows.Forms.Panel
	End Class
End Namespace
