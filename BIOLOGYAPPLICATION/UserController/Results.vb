Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Drawing
Imports System.Data
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Windows.Forms
Imports System.Drawing.Printing

Namespace USERCONTROLS
	Public Partial Class UCTestResult
		Inherits UserControl

		Private linesPrinted As Integer
		Private lines As String()

		Public Event GoToLessons As EventHandler

		Public Sub New()
			InitializeComponent()
		End Sub

		Private Sub btnBackToLessons_Click(sender As Object, e As EventArgs)
			RaiseEvent GoToLessons(sender, e)
		End Sub

		' Print Event
		Private Sub miPrint_Click(sender As Object, e As System.EventArgs)
			If printDialog1.ShowDialog() = DialogResult.OK Then
				printDocument1.Print()
			End If
		End Sub

		' OnBeginPrint
        Private Sub OnBeginPrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
            Try
                Dim param As Char() = {ControlChars.Lf}

                If printDialog1.PrinterSettings.PrintRange = PrintRange.Selection Then
                    lines = RTResults.SelectedText.Split(param)
                Else
                    lines = RTResults.Text.Split(param)
                End If

                Dim i As Integer = 0
                Dim trimParam As Char() = {ControlChars.Cr}
                For Each s As String In lines
                    lines(System.Math.Max(System.Threading.Interlocked.Increment(i), i - 1)) = s.TrimEnd(trimParam)
                Next
            Catch ex As Exception

            End Try

        End Sub
		' OnPrintPage
		Private Sub OnPrintPage(sender As Object, e As System.Drawing.Printing.PrintPageEventArgs)
			Dim x As Integer = e.MarginBounds.Left
			Dim y As Integer = e.MarginBounds.Top
			Dim brush As Brush = New SolidBrush(RTResults.ForeColor)

			While linesPrinted < lines.Length
				e.Graphics.DrawString(lines(System.Math.Max(System.Threading.Interlocked.Increment(linesPrinted),linesPrinted - 1)), RTResults.Font, brush, x, y)
				y += 15
				If y >= e.MarginBounds.Bottom Then
					e.HasMorePages = True
					Return
				End If
			End While

			linesPrinted = 0
			e.HasMorePages = False
		End Sub

		Private Sub btnPrint_Click(sender As Object, e As EventArgs)
			printPreviewDialog1.ShowDialog()
		End Sub

		Private Sub txtWrongAnswer_TextChanged(sender As Object, e As EventArgs)

		End Sub
	End Class
End Namespace
