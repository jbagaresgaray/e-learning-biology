Partial Class Test
	''' <summary>
	''' Required designer variable.
	''' </summary>
	Private components As System.ComponentModel.IContainer = Nothing

	''' <summary>
	''' Clean up any resources being used.
	''' </summary>
	''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	Protected Overrides Sub Dispose(disposing As Boolean)
		If disposing AndAlso (components IsNot Nothing) Then
			components.Dispose()
		End If
		MyBase.Dispose(disposing)
	End Sub

	#Region "Component Designer generated code"

	''' <summary>
	''' Required method for Designer support - do not modify
	''' the contents of this method with the code editor.
	''' </summary>
	Private Sub InitializeComponent()
		Me.txtQuestion = New System.Windows.Forms.TextBox()
		Me.btnNextQuestion = New System.Windows.Forms.Button()
		Me.btnExitTest = New System.Windows.Forms.Button()
		Me.FlowLayoutAnswerPanel = New System.Windows.Forms.FlowLayoutPanel()
		Me.txtLessonId = New System.Windows.Forms.TextBox()
		Me.SuspendLayout()
		'
		' txtQuestion
		'
		Me.txtQuestion.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.txtQuestion.Font = New System.Drawing.Font("Georgia", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
		Me.txtQuestion.Location = New System.Drawing.Point(29, 12)
		Me.txtQuestion.Multiline = True
		Me.txtQuestion.Name = "txtQuestion"
		Me.txtQuestion.[ReadOnly] = True
		Me.txtQuestion.Size = New System.Drawing.Size(549, 59)
		Me.txtQuestion.TabIndex = 1
		Me.txtQuestion.Text = "You will have to make sure the size is set appropriately, or for it to have an ap" & "propriate dockfill.?"
		'
		' btnNextQuestion
		'
		Me.btnNextQuestion.Cursor = System.Windows.Forms.Cursors.Hand
		Me.btnNextQuestion.Font = New System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
		Me.btnNextQuestion.Location = New System.Drawing.Point(80, 285)
		Me.btnNextQuestion.Name = "btnNextQuestion"
		Me.btnNextQuestion.Size = New System.Drawing.Size(85, 27)
		Me.btnNextQuestion.TabIndex = 6
		Me.btnNextQuestion.Text = "Next Question"
		Me.btnNextQuestion.UseVisualStyleBackColor = True
		AddHandler Me.btnNextQuestion.Click, New System.EventHandler(AddressOf Me.btnNextQuestion_Click)
		'
		' btnExitTest
		'
		Me.btnExitTest.Cursor = System.Windows.Forms.Cursors.Hand
		Me.btnExitTest.Font = New System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
		Me.btnExitTest.Location = New System.Drawing.Point(28, 285)
		Me.btnExitTest.Name = "btnExitTest"
		Me.btnExitTest.Size = New System.Drawing.Size(52, 27)
		Me.btnExitTest.TabIndex = 7
		Me.btnExitTest.Text = "Exit"
		Me.btnExitTest.UseVisualStyleBackColor = True
		AddHandler Me.btnExitTest.Click, New System.EventHandler(AddressOf Me.btnExitTest_Click)
		'
		' FlowLayoutAnswerPanel
		'
		Me.FlowLayoutAnswerPanel.AutoScroll = True
		Me.FlowLayoutAnswerPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.FlowLayoutAnswerPanel.Font = New System.Drawing.Font("Georgia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CByte(0))
		Me.FlowLayoutAnswerPanel.Location = New System.Drawing.Point(28, 78)
		Me.FlowLayoutAnswerPanel.Name = "FlowLayoutAnswerPanel"
		Me.FlowLayoutAnswerPanel.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
		Me.FlowLayoutAnswerPanel.Size = New System.Drawing.Size(550, 177)
		Me.FlowLayoutAnswerPanel.TabIndex = 8
		'
		' txtLessonId
		'
		Me.txtLessonId.Location = New System.Drawing.Point(558, 33)
		Me.txtLessonId.Name = "txtLessonId"
		Me.txtLessonId.Size = New System.Drawing.Size(10, 20)
		Me.txtLessonId.TabIndex = 9
		Me.txtLessonId.Visible = False
		'
		' UCTest
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6F, 13F)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.ControlLightLight
		Me.Controls.Add(Me.FlowLayoutAnswerPanel)
		Me.Controls.Add(Me.btnExitTest)
		Me.Controls.Add(Me.btnNextQuestion)
		Me.Controls.Add(Me.txtQuestion)
		Me.Controls.Add(Me.txtLessonId)
		Me.Name = "UCTest"
		Me.Size = New System.Drawing.Size(599, 323)
		AddHandler Me.Load, New System.EventHandler(AddressOf Me.UCTest_Load)
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

	#End Region

	Public txtQuestion As System.Windows.Forms.TextBox
	Public btnNextQuestion As System.Windows.Forms.Button
	Public btnExitTest As System.Windows.Forms.Button
	Private FlowLayoutAnswerPanel As System.Windows.Forms.FlowLayoutPanel
	Public txtLessonId As System.Windows.Forms.TextBox
End Class
