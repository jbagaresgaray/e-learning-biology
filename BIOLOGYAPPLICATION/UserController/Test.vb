Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Drawing
Imports System.Data
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Windows.Forms

'Public Delegate Sub CollectResults(results As UserResults)

Partial Public Class Test
    Inherits UserControl
    Public Event ExitTest As EventHandler

    Public Event ShowFinalResult As EventHandler

    'Public Event collectResults As CollectResults

    Private Questions As New List(Of QuestionsInfo)()

    Private Answers As New List(Of Answer)()

    Private iCorrectAnswers As New List(Of String)()

    Private sCorrectAnswers As New List(Of String)()

    Private sWrongAnswers As New List(Of String)()

    Private UserResult As New UserResults()

    Private currentAnswer As String = String.Empty

    Private lessonId As String

    Private questionIndex As Integer

    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub btnExitTest_Click(sender As Object, e As EventArgs)
        RaiseEvent ExitTest(sender, e)
    End Sub

    Private Sub UCTest_Load(sender As Object, e As EventArgs)
        questionIndex = 0

        'Questions = GetQuestions(txtLessonId.Text).OrderBy(Function(a) Guid.NewGuid()).ToList()
        'randomize questions
        CollectQuestionAndAnswers(0)
        ' Get the first question array
    End Sub


    Private Sub btnNextQuestion_Click(sender As Object, e As EventArgs)
        Try
            Dim radioButtons = FlowLayoutAnswerPanel.Controls.OfType(Of RadioButton)().ToList()
            Dim rdCounter = 0
            For Each rdButton As RadioButton In radioButtons
                If rdButton.Checked = False Then
                    rdCounter += 1
                End If
            Next

            If radioButtons.Count = rdCounter Then
                System.Windows.Forms.MessageBox.Show("Please select an answer", "Information", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information)
                Return
            End If

            'collect correct answer
            Dim checkedButton = FlowLayoutAnswerPanel.Controls.OfType(Of RadioButton)().FirstOrDefault(Function(r) r.Checked)

            If currentAnswer = checkedButton.Name Then
                CollectCorrectAnswers(checkedButton.Text)
                CountCorrectAnswer(checkedButton.Name.ToString())
            Else
                CollectWrongAnswers(checkedButton.Text)
            End If

            questionIndex += 1

            Dim questionCounter As Integer = Questions.Count

            If questionIndex >= questionCounter Then
                'string[] results = new string[] { this.iCorrectAnswers.Count.ToString(), Convert.ToString(Convert.ToInt32(Questions.Count.ToString()) - Convert.ToInt32(this.iCorrectAnswers.Count.ToString())) };

                Dim userresult As New UserResults()
                If Me.sCorrectAnswers.Count > 0 Then
                    'userresult.correctAnswers = Me.sCorrectAnswers.ToArray()
                End If
                If Me.sWrongAnswers.Count > 0 Then
                    'userresult.wrongAnswers = Me.sWrongAnswers.ToArray()
                End If

                'RaiseEvent collectResults(userresult)

                RaiseEvent ShowFinalResult(sender, e)
            Else
                CollectQuestionAndAnswers(questionIndex)
            End If
            '
        Catch generatedExceptionName As Exception
        End Try

    End Sub

    Private Sub CountCorrectAnswer(radioName As String)
        Me.iCorrectAnswers.Add(radioName)

    End Sub

    ''' <summary>
    ''' GET CORRECT ANSWERS
    ''' </summary>
    ''' <param name="answerText"></param>
    Private Sub CollectCorrectAnswers(answerText As String)
        Me.sCorrectAnswers.Add(answerText.ToString())
    End Sub

    ''' <summary>
    ''' GET WRONG ANSWERS
    ''' </summary>
    ''' <param name="answerText"></param>
    Private Sub CollectWrongAnswers(answerText As String)
        Me.sWrongAnswers.Add(answerText.ToString())
    End Sub

    ' Delegate call back function
    Public Sub GetCurrentLessonIdFromMain(lessonId As String)
        Me.lessonId = lessonId
    End Sub

    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="index"></param>
    Private Sub CollectQuestionAndAnswers(index As Integer)
        Try
            If Questions IsNot Nothing Then
                txtQuestion.Text = Questions(index).questionTitle.ToString()

                Dim answers As List(Of Answer) = GetAnswers(Questions(index).questionid.ToString())
                FlowLayoutAnswerPanel.Controls.Clear()
                For Each answer As Answer In answers
                    Dim radioButton As New RadioButton()
                    radioButton.Text = answer.AnswerText
                    radioButton.Width = 300
                    radioButton.Height = 80
                    radioButton.Cursor = Cursors.Hand
                    radioButton.Checked = False
                    If answer.isCorrect = True Then
                        currentAnswer = answer.AnswerText
                    End If
                    radioButton.Name = answer.AnswerText

                    FlowLayoutAnswerPanel.Controls.Add(radioButton)
                Next
            End If
            '
        Catch generatedExceptionName As Exception
        End Try
    End Sub
End Class
