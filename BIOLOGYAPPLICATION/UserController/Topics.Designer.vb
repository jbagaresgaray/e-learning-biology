Partial Class UCTopics
	''' <summary>
	''' Required designer variable.
	''' </summary>
	Private components As System.ComponentModel.IContainer = Nothing

	''' <summary>
	''' Clean up any resources being used.
	''' </summary>
	''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	Protected Overrides Sub Dispose(disposing As Boolean)
		If disposing AndAlso (components IsNot Nothing) Then
			components.Dispose()
		End If
		MyBase.Dispose(disposing)
	End Sub

	#Region "Component Designer generated code"

	''' <summary>
	''' Required method for Designer support - do not modify
	''' the contents of this method with the code editor.
	''' </summary>
	Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UCTopics))
        Me.txtLessonId = New System.Windows.Forms.TextBox()
        Me.panel3 = New System.Windows.Forms.Panel()
        Me.btnTakeTheTest = New BIOLOGYAPPLICATION.FlatButton()
        Me.groupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtTitle = New System.Windows.Forms.TextBox()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.panel1 = New System.Windows.Forms.Panel()
        Me.photo = New System.Windows.Forms.PictureBox()
        Me.panel2 = New System.Windows.Forms.Panel()
        Me.panel3.SuspendLayout()
        Me.panel1.SuspendLayout()
        CType(Me.photo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtLessonId
        '
        Me.txtLessonId.Location = New System.Drawing.Point(612, 10)
        Me.txtLessonId.Name = "txtLessonId"
        Me.txtLessonId.Size = New System.Drawing.Size(26, 21)
        Me.txtLessonId.TabIndex = 9
        Me.txtLessonId.Visible = False
        '
        'panel3
        '
        Me.panel3.Controls.Add(Me.btnTakeTheTest)
        Me.panel3.Controls.Add(Me.groupBox1)
        Me.panel3.Controls.Add(Me.txtLessonId)
        Me.panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panel3.Location = New System.Drawing.Point(0, 243)
        Me.panel3.Name = "panel3"
        Me.panel3.Size = New System.Drawing.Size(757, 53)
        Me.panel3.TabIndex = 11
        '
        'btnTakeTheTest
        '
        Me.btnTakeTheTest.BackColor = System.Drawing.Color.Transparent
        Me.btnTakeTheTest.BaseColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.btnTakeTheTest.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnTakeTheTest.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTakeTheTest.Location = New System.Drawing.Point(15, 10)
        Me.btnTakeTheTest.Name = "btnTakeTheTest"
        Me.btnTakeTheTest.Rounded = True
        Me.btnTakeTheTest.Size = New System.Drawing.Size(106, 32)
        Me.btnTakeTheTest.TabIndex = 11
        Me.btnTakeTheTest.Text = "View Lesson"
        Me.btnTakeTheTest.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'groupBox1
        '
        Me.groupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupBox1.Location = New System.Drawing.Point(-3, 48)
        Me.groupBox1.Name = "groupBox1"
        Me.groupBox1.Size = New System.Drawing.Size(754, 5)
        Me.groupBox1.TabIndex = 10
        Me.groupBox1.TabStop = False
        '
        'txtTitle
        '
        Me.txtTitle.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTitle.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtTitle.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTitle.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTitle.Location = New System.Drawing.Point(15, 3)
        Me.txtTitle.Multiline = True
        Me.txtTitle.Name = "txtTitle"
        Me.txtTitle.ReadOnly = True
        Me.txtTitle.Size = New System.Drawing.Size(736, 70)
        Me.txtTitle.TabIndex = 13
        Me.txtTitle.TabStop = False
        Me.txtTitle.Text = "You will have to make sure the size is set appropriately, or for it to have an ap" & _
    "propriate dockfill."
        '
        'txtDescription
        '
        Me.txtDescription.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtDescription.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDescription.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtDescription.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtDescription.Location = New System.Drawing.Point(0, 0)
        Me.txtDescription.MaxLength = 9999999
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ReadOnly = True
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(528, 205)
        Me.txtDescription.TabIndex = 3
        Me.txtDescription.TabStop = False
        Me.txtDescription.Text = resources.GetString("txtDescription.Text")
        '
        'panel1
        '
        Me.panel1.Controls.Add(Me.photo)
        Me.panel1.Location = New System.Drawing.Point(15, 34)
        Me.panel1.Name = "panel1"
        Me.panel1.Size = New System.Drawing.Size(205, 177)
        Me.panel1.TabIndex = 14
        '
        'photo
        '
        Me.photo.BackgroundImage = CType(resources.GetObject("photo.BackgroundImage"), System.Drawing.Image)
        Me.photo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.photo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.photo.Location = New System.Drawing.Point(10, 9)
        Me.photo.Name = "photo"
        Me.photo.Size = New System.Drawing.Size(187, 158)
        Me.photo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.photo.TabIndex = 11
        Me.photo.TabStop = False
        '
        'panel2
        '
        Me.panel2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel2.AutoScroll = True
        Me.panel2.AutoSize = True
        Me.panel2.Controls.Add(Me.txtDescription)
        Me.panel2.Location = New System.Drawing.Point(226, 32)
        Me.panel2.Name = "panel2"
        Me.panel2.Size = New System.Drawing.Size(528, 205)
        Me.panel2.TabIndex = 15
        '
        'UCTopics
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.panel2)
        Me.Controls.Add(Me.panel1)
        Me.Controls.Add(Me.panel3)
        Me.Controls.Add(Me.txtTitle)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "UCTopics"
        Me.Size = New System.Drawing.Size(757, 296)
        Me.panel3.ResumeLayout(False)
        Me.panel3.PerformLayout()
        Me.panel1.ResumeLayout(False)
        CType(Me.photo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panel2.ResumeLayout(False)
        Me.panel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

	#End Region

	Public txtLessonId As System.Windows.Forms.TextBox
	Private panel3 As System.Windows.Forms.Panel
    Public txtTitle As System.Windows.Forms.TextBox
	Private groupBox1 As System.Windows.Forms.GroupBox
	Public photo As System.Windows.Forms.PictureBox
	Public txtDescription As System.Windows.Forms.TextBox
	Private panel1 As System.Windows.Forms.Panel
    Private panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnTakeTheTest As BIOLOGYAPPLICATION.FlatButton



End Class
