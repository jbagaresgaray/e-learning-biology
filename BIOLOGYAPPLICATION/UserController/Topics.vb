Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Drawing
Imports System.Data
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Windows.Forms

Public Delegate Function PassCurrentLessonIdToMain(currentLessonId As String) As String

Public Partial Class UCTopics
	Inherits UserControl
	Public passCurrentLessonIdToMain As PassCurrentLessonIdToMain

	Public Event HideAllLessonContent As EventHandler


	Public Property lessonId() As String
		Get
			Return m_lessonId
		End Get
		Set
			m_lessonId = Value
		End Set
	End Property
	Private m_lessonId As String

	Public Sub New()
		InitializeComponent()
	End Sub

    Private Sub btnTakeTheTest_Click(sender As Object, e As EventArgs) Handles btnTakeTheTest.Click
        passCurrentLessonIdToMain(txtLessonId.Text)

        'RaiseEvent HideAllLessonContent(sender, e)

        Dim frm As New frmLessonDetail
        frm.ShowForm()
    End Sub

    Private Sub UCTopics_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class
